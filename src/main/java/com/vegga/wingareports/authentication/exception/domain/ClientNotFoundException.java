package com.vegga.wingareports.authentication.exception.domain;

public class ClientNotFoundException extends Exception {
    public ClientNotFoundException(String message) {
        super(message);
    }
}
