package com.vegga.wingareports.authentication.service;

import com.vegga.wingareports.authentication.exception.domain.*;
import com.vegga.wingareports.entity.ClientUser;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface AuthenticationService {

    public ClientUser register(ClientUser user) throws UserNotFoundException, UsernameExistException, EmailExistException, ClientNotFoundException;

    List<ClientUser> getUsers();

    ClientUser findUserByUsername(String username);

    ClientUser findUserByEmail(String email);

    ClientUser addNewUser(String firstName, String lastName, String username, String email, String role, boolean isNonLocked, boolean isActive, MultipartFile profileImage) throws UserNotFoundException, UsernameExistException, EmailExistException, IOException, NotAnImageFileException;

    ClientUser updateUser(String currentUsername, String newFirstName, String newLastName, String newUsername, String newEmail, String role, boolean isNonLocked, boolean isActive, MultipartFile profileImage) throws UserNotFoundException, UsernameExistException, EmailExistException, IOException, NotAnImageFileException;

    void deleteUser(String username) throws IOException;

    void resetPassword(String email) throws MessagingException, EmailNotFoundException;

    ClientUser updateProfileImage(String username, MultipartFile profileImage) throws UserNotFoundException, UsernameExistException, EmailExistException, IOException, NotAnImageFileException;

}
