package com.vegga.wingareports.authentication.controller;

import com.vegga.wingareports.authentication.exception.ExceptionHandling;
import com.vegga.wingareports.authentication.exception.domain.ClientNotFoundException;
import com.vegga.wingareports.authentication.exception.domain.EmailExistException;
import com.vegga.wingareports.authentication.exception.domain.UserNotFoundException;
import com.vegga.wingareports.authentication.exception.domain.UsernameExistException;
import com.vegga.wingareports.authentication.model.HttpResponse;
import com.vegga.wingareports.authentication.model.UserPrincipal;
import com.vegga.wingareports.authentication.service.AuthenticationService;
import com.vegga.wingareports.authentication.utility.JWTTokenProvider;
import com.vegga.wingareports.entity.ClientUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.vegga.wingareports.authentication.constant.SecurityConstant.JWT_TOKEN_HEADER;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(path = { "/auth/user"})
@CrossOrigin
public class AuthenticationController extends ExceptionHandling {

    public static final String EMAIL_SENT = "An email with a new password was sent to: ";
    public static final String USER_DELETED_SUCCESSFULLY = "User deleted successfully";
    private AuthenticationManager authenticationManager;
    private AuthenticationService authenticationService;
    private JWTTokenProvider jwtTokenProvider;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, AuthenticationService authenticationService, JWTTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.authenticationService = authenticationService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping("/register")
    public ResponseEntity<ClientUser> register(@RequestBody ClientUser user) throws UserNotFoundException, UsernameExistException, EmailExistException, MessagingException, ClientNotFoundException {
        ClientUser newUser = authenticationService.register(user);
        return new ResponseEntity<>(newUser, OK);
    }

    @PostMapping("/login")
    public ResponseEntity<Map<String, Object>> login(@RequestBody ClientUser user) {
        authenticate(user.getUsername(), user.getPassword());
        ClientUser loginUser = authenticationService.findUserByUsername(user.getUsername());
        Map<String, Object> response = new HashMap<>();
        response.put("user", loginUser);
        UserPrincipal userPrincipal = new UserPrincipal(loginUser);
        Map<String, String> tokens = new HashMap<>();
        tokens.put("access-token", jwtTokenProvider.generateJwtToken(userPrincipal));
        response.put("tokens", tokens);
        return ResponseEntity.ok(response);
//        HttpHeaders jwtHeader = getJwtHeader(userPrincipal);
//        return new ResponseEntity<>(loginUser, jwtHeader, OK);
    }

    @GetMapping("/find/{username}")
    public ResponseEntity<ClientUser> getUser(@PathVariable("username") String username) {
        ClientUser user = authenticationService.findUserByUsername(username);
        return new ResponseEntity<>(user, OK);
    }

    @GetMapping("/list")
    public ResponseEntity<List<ClientUser>> getAllUsers() {
        List<ClientUser> users = authenticationService.getUsers();
        return new ResponseEntity<>(users, OK);
    }

    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new HttpResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase().toUpperCase(),
                message), httpStatus);
    }

    private HttpHeaders getJwtHeader(UserPrincipal user) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(JWT_TOKEN_HEADER, jwtTokenProvider.generateJwtToken(user));
        return headers;
    }

    private void authenticate(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }
}
