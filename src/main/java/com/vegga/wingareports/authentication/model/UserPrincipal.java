package com.vegga.wingareports.authentication.model;

import com.vegga.wingareports.entity.ClientUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public class UserPrincipal implements UserDetails {
    private ClientUser clientUser;

    public UserPrincipal(ClientUser user) {
        this.clientUser = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return stream(this.clientUser.getAuthorities()).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return this.clientUser.getPassword();
    }

    @Override
    public String getUsername() {
        return this.clientUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return clientUser.isActive();
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.clientUser.isNotLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return clientUser.isActive();
    }

    @Override
    public boolean isEnabled() {
        return this.clientUser.isActive();
    }
}
