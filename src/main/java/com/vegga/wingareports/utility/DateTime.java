 /**
  		 * Copyright (c) 2015, EightFolds its affiliates. All rights reserved.
  		 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
  		 *
 */

package com.vegga.wingareports.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

 /**
  * This class consists exclusively of  static  methods that operate on or return
  * date or string .  It contains polymorphic algorithms that operate on
  * date,  which return a new date or string  backed by a
  * specified collection, and a few other odds and ends.
 */

 public class DateTime {

     public static TimeZone getTimeZoneFromOffset(int offset) {
         ZoneId zoneId = ZoneId.ofOffset("GMT", ZoneOffset.ofTotalSeconds(offset));
         TimeZone timeZone = TimeZone.getTimeZone(zoneId);
         return timeZone;
     }

     public static Date parseDateTime(String time) throws ParseException {
         return parseDateTime(time, "yyyy-MM-dd HH:mm:ss", null);
     }

     public static Date parseDateTime(String time, TimeZone timeZone) throws ParseException {
         return parseDateTime(time, "yyyy-MM-dd HH:mm:ss", timeZone);
     }

     public static Date parseDateTime(String time, String formatPattern, TimeZone timeZone) throws ParseException {
         /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatPattern);
         LocalDateTime localDateTime = LocalDateTime.parse(time, formatter);
         ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.ofOffset("GMT", ZoneOffset.ofTotalSeconds(timeZone.getRawOffset() / 1000)));
         return Date.from(zonedDateTime.toInstant());*/

         SimpleDateFormat format = new SimpleDateFormat(formatPattern);
         if(timeZone != null) {
             format.setTimeZone(timeZone);
         }
         return format.parse(time);
     }

     public static String formatDateTime(Date time) {
         return formatDateTime(time, "yyyy-MM-dd HH:mm:ss");
     }

     public static String formatDateTime(Date time, String formatPattern) {
         SimpleDateFormat format = new SimpleDateFormat(formatPattern);
         return format.format(time);
     }

     public static Date addDays(Date date, int days) {
         GregorianCalendar cal = new GregorianCalendar();
         cal.setTime(date);
         cal.add(Calendar.DATE, days);

         return cal.getTime();
     }

     public static Date getTodayStartOfDay() throws ParseException {
         return getTodayStartOfDay(null);
     }

     public static Date getTodayStartOfDay(TimeZone tz) throws ParseException {
         Calendar now = tz != null ? Calendar.getInstance(tz) : Calendar.getInstance();
         now.set(Calendar.HOUR_OF_DAY, 0);
         now.set(Calendar.MINUTE, 0);
         now.set(Calendar.SECOND, 0);
         return now.getTime();
     }

     public static Date getTodayCalenderDate(TimeZone tz) throws ParseException {
         Calendar calendar = Calendar.getInstance(tz);
         return calendar.getTime();
     }

     public static String getTodayDate(TimeZone tz) throws ParseException {
         return getTodayDate(tz , "MMM d, yyyy" );
     }

     public static String getTodayDate(TimeZone tz, String formatPattern) throws ParseException {
         Calendar now = Calendar.getInstance(tz);
         now.set(Calendar.HOUR_OF_DAY, 0);
         now.set(Calendar.MINUTE, 0);
         now.set(Calendar.SECOND, 0);

         SimpleDateFormat sdf = new SimpleDateFormat(formatPattern != null ? formatPattern : "MMM d, yyyy");
         sdf.setTimeZone(tz);
         return sdf.format(new Date(now.getTimeInMillis()));
     }


     public static Date getTodayEndOfDay() throws ParseException {
         return getTodayEndOfDay(null);
     }

     public static Date getTodayEndOfDay(TimeZone tz) throws ParseException {
         Calendar now = tz != null ? Calendar.getInstance(tz) : Calendar.getInstance();
         now.set(Calendar.HOUR_OF_DAY, 23);
         now.set(Calendar.MINUTE, 59);
         now.set(Calendar.SECOND, 59);

         return now.getTime();
     }

 /**

      *  method  convert millisecond to string date
      *  @param time				time in long millisecond
      *  @return 				date for that param in string format yyyy-MM-dd HH:mm:ss
  */
     public static String getFromMillies(long time) {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         return format.format(new Date(time));
     }

 /**
      * method  convert millisecond to string date in specified format
      *  @param time				time in long millisecond
      *  @format					specified format
      *  @return 				date for that param in string and specified format
  */

     public static String getFromMillies(long time, String format) {
         SimpleDateFormat sdf = new SimpleDateFormat(format);
         return sdf.format(new Date(time));
     }

 /**
      * method  convert millisecond to string date in utc
      *  @param time				time in long millisecond
      *  @return 				date for that param in string in default yyyy-MM-dd HH:mm:ss in utc
  */

     public static String getFromMilliesInUTC(long time) {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         format.setTimeZone(TimeZone.getTimeZone("GMT"));
         return format.format(new Date(time));
     }
 /**
      * method  convert string date to millisecond in utc
      *  @param time				takes time in string format
      *  @return 				return millisecond in long  in utc
      *  @exception ParseException
  */

     public static long getInMilliesFromUTC(String time) throws ParseException {
         return getInMilliesFromUTC(time, "yyyy-MM-dd HH:mm:ss");
     }

     public static long getInMilliesFromUTC(String time, String pattern) throws ParseException {
         SimpleDateFormat format = new SimpleDateFormat(pattern);
         format.setTimeZone(TimeZone.getTimeZone("GMT"));
         return format.parse(time).getTime();
     }

 /**
      * * method  convert string date to millisecond
      *  @param time				takes time in string format
      *  @return 				return millisecond in long
      *  @exception ParseException
  */

     public static long getInMillies(String time) throws ParseException {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         return format.parse(time).getTime();
     }

 /**
      * 	method return  difference between two dates in millisecond
      *  @param time1,time2		takes two date parameter in string format is in default yyyy-MM-dd HH:mm:ss format
      *  @return 				return date difference in millisecond
      *  @exception ParseException
  */

     public static long getTimeDiffInMillies(String time1, String time2) throws ParseException {
         return getTimeDiffInMillies(time1, time2, "yyyy-MM-dd HH:mm:ss");
     }

 /**
      *	method return  difference between two dates in millisecond in specified  format
      *  @param time1,time2		takes two date parameter in string format specified in format as parameter
      *  @return 				return date difference in millisecond
      *  @exception ParseException
  */

     public static long getTimeDiffInMillies(String time1, String time2, String format) throws ParseException {
         SimpleDateFormat sdf = new SimpleDateFormat(format);
         long t1= sdf.parse(time1).getTime();
         long t2= sdf.parse(time2).getTime();
         return t1-t2;
     }

 /**
      * 	method  for add time in millisecond
      *  @param time1			time in string format in which the next parameter
      *  @param toAdd			how much time is added in millisecond
      *  @return 				return date difference in millisecond
      *  @exception ParseException
  */

     public static String addTime(String time, long toAdd) throws ParseException {
         return addTime(time, toAdd, "yyyy-MM-dd HH:mm:ss");
     }


 /**
      *  method   add time for specified format
      *  @param time1			time in string format in which the next parameter is added  in specified format
      *  @param toAdd			how much time is added in millisecond
      *  @return 				return date difference in millisecond
      *  @exception ParseException
  */

     public static String addTime(String time, long toAdd, String format) throws ParseException {
         SimpleDateFormat sdf = new SimpleDateFormat(format);
         long t= sdf.parse(time).getTime();
         t += toAdd;
         return sdf.format(new Date(t));
     }

 /**
      *  method   for current datetime in utc
      *  @return 		return current date and time in utc
  */

     public static String getNowInUTC() {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         format.setTimeZone(TimeZone.getTimeZone("GMT"));
         return format.format(new Date(System.currentTimeMillis()));
     }

     public static String getCurrentDateInUTC() {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         format.setTimeZone(TimeZone.getTimeZone("GMT"));
         return format.format(new Date(System.currentTimeMillis()));
     }

 /**
      * method   for current datetime
      *  @return 		return current date and time in local time zone
  */

     public static String getNow() {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         return format.format(new Date(System.currentTimeMillis()));
     }

 /**
      *  * method   for current date
      *  @return 		return current date
  */

     public static String getCurrentDate() {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         return format.format(new Date(System.currentTimeMillis()));
     }

 /**
      *  * method   for current timezone
      *  @return 		return current time zone
  */

     public static String getCurrentTimeZone() {
         TimeZone timeZone = TimeZone.getDefault();
         return timeZone.getID();
     }

 /**
      *  * method   for  getting date part from datetime
      * 	@param datetime 		takes datime in string with yyyy-MM-dd HH:mm:ss format
      *  @return 				return date part
      *  @exception ParseException
  */

     public static String getDate(String datetime) throws ParseException {
         SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
         return format2.format(format1.parse(datetime));
     }

 /**
      *  * method   for current time
      * 	@param  datetime					takes datime in string with yyyy-MM-dd HH:mm:ss format
      *  @return 							return time part
      *  @exception ParseException
  */


     public static String getTime(String datetime) throws ParseException {
         SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         SimpleDateFormat format2 = new SimpleDateFormat("HH:mm:ss");
         return format2.format(format1.parse(datetime));
     }

 /**
      * 	method  to convert from one format of date into another
      * 	@param datetime 					takes date in string format
      * 	@param formatForm					format in which the parameter date is
      * 	@param formatTo						format in which you want to convert
      *  @return 							return converted date format
      *  @exception ParseException
  */


     public static String getDateTime(String datetime, String formatFrom, String formatTo) throws ParseException {
         SimpleDateFormat sdf1 = new SimpleDateFormat(formatFrom);
         SimpleDateFormat sdf2 = new SimpleDateFormat(formatTo);
         return sdf2.format(sdf1.parse(datetime));
     }

 /**
      * 	method  to convert date from one timezone to another  in default yyyy-MM-dd HH:mm:ss format
      * 	@param datetime 					takes date time in string format
      * 	@param originalTimezone				timezone of parameter
      * 	@param formatTo						timezone in which you want convert
      *  @return 							return converted date
      *  @exception ParseException
  */

     public static String convertDateTimeBwTimeZone(String datetime, String originalTimezone, String finalTimeZone) throws ParseException {
         String patternString = "yyyy-MM-dd HH:mm:ss";
         return convertDateTimeBwTimeZone(datetime, patternString, originalTimezone, finalTimeZone);
     }

     public static String convertBwTimeZone(String datetime, String originalTimezone, String finalTimeZone) {
         String patternString = "yyyy-MM-dd HH:mm:ss";
         try {
             return convertDateTimeBwTimeZone(datetime, patternString, originalTimezone, finalTimeZone);
         } catch (ParseException e) {
             e.printStackTrace();
         }
         return "";
     }

 /**
      * 	method  to convert date from one timezone to another  in specified format
      * 	@param datetime 					takes date time in string format
      *  @param patternString				format of parameter date time
      * 	@param originalTimezone				timezone of parameter
      * 	@param formatTo						timezone in which you want convert
      *  @return 							return converted date  in string
      *  @exception ParseException
  */

     public static String convertDateTimeBwTimeZone(String datetime, String patternString, String originalTimezone, String finalTimeZone) throws ParseException {
         try {
             DateFormat originalFormate = new SimpleDateFormat(patternString);
             originalFormate.setTimeZone(TimeZone.getTimeZone(originalTimezone));

             DateFormat finalFormat = new SimpleDateFormat(patternString);
             finalFormat.setTimeZone(TimeZone.getTimeZone(finalTimeZone));

             Date timestamp = originalFormate.parse(datetime);
             String output = finalFormat.format(timestamp);
             return output;
         } catch (Exception e) {
             e.printStackTrace();
         }

         return datetime;
     }

 /**
      *  method return  today date
      *  @return 			return  today date in yyyy-MM-dd format
 */

     public static String getToday() {
         Calendar calendar = Calendar.getInstance();

         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

         return format.format(calendar.getTime());
     }
 /**
      *  method to get current month first date
      *  @return 			return  current month first  date  in yyyy-MM-dd format
 */
     public static String getThisMonthFirstDate() {
         Calendar calendar = Calendar.getInstance();
         calendar.set(Calendar.DAY_OF_MONTH, 1);

         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

         return format.format(calendar.getTime());
     }

 /**
      * method to get current month last date
      *  @return 			return  current month last  date in yyyy-MM-dd format
 */

     public static String getThisMonthLastDate() {
         Calendar calendar = Calendar.getInstance();
         calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

         return format.format(calendar.getTime());
     }

 /**
      *  method to get   month first date of specified date
      *  @return 				return  specified parameter  month first  date in yyyy-MM-dd format
      *  @exception ParseException
 */
     public static String getMonthFirstDate(String date) throws ParseException {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

         Calendar calendar = Calendar.getInstance();
         calendar.setTime(format.parse(date));
         calendar.set(Calendar.DAY_OF_MONTH, 1);

         return format.format(calendar.getTime());
     }

 /**
      * method to get   month last date of specified date
      *  @return 				return  specified parameter  month last  day in yyyy-MM-dd format
      *  @exception ParseException
 */
     public static String getMonthLastDate(String date) throws ParseException {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

         Calendar calendar = Calendar.getInstance();
         calendar.setTime(format.parse(date));
         calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

         return format.format(calendar.getTime());
     }

 /**
      * 	method return one day previous date of specified date
      * 	@param				String in date format
      *  @return 			return   date in string format
      *  @exception ParseException
 */

     public static String getPrevDate(String date) throws ParseException {
         DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         Calendar calendar = Calendar.getInstance();
         calendar.setTime(format.parse(date));
         calendar.add(Calendar.DATE, -1);
         return format.format(calendar.getTime());
     }

 /**
      * 	method return one day next date of specified date
      * 	@param				String in date format
      *  @return 			return   date in string format
      *  @exception ParseException
 */
     public static String getNextDate(String date) throws ParseException {
         DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         Calendar calendar = Calendar.getInstance();
         calendar.setTime(format.parse(date));
         calendar.add(Calendar.DATE, 1);
         return format.format(calendar.getTime());
     }

 /**
      * 	method return  the difference day count between two dates
      * 	@param format			 format of date
      *  @return fromDate 		 from date
      *  @return fromDate 		 to date
      *  @exception ParseException
 */

     public static int getDaysCountBetweenDates(String format, String fromDate, String toDate) throws ParseException {

              SimpleDateFormat formatter =  new SimpleDateFormat(format);
             Date aDate = formatter.parse(fromDate);
             Date bDate = formatter.parse(toDate);

             Calendar with = Calendar.getInstance();
             with.setTime(aDate);
             Calendar to = Calendar.getInstance();
             to.setTime(bDate);
             to.set(Calendar.YEAR, with.get(Calendar.YEAR));

             int withDAY = with.get(Calendar.DAY_OF_YEAR);

             int toDAY = to.get(Calendar.DAY_OF_YEAR);

             int diffDay =  toDAY  - withDAY;

             return diffDay;
     }


     /**
      *  method   for current date in given time zone
      *  @return  return current date in given time zone
  */

     public static String getCurrentDateByTimeZone(String timeZone) {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         format.setTimeZone(TimeZone.getTimeZone(timeZone));
         return format.format(new Date(System.currentTimeMillis()));
     }

     public static long getInTimeMillies(String time) throws ParseException {
         SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
         return format.parse(time).getTime();
     }
     public static long getDateInMillies(String date) throws ParseException {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         return format.parse(date).getTime();
     }

     public static long getDateTimeInMillies(String date) throws ParseException {
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         return format.parse(date).getTime();
     }

     public static int convertDayToMinutes(int noOfDays) {
         return noOfDays * 1440;
     }

     public static String getCurrentDateTimeWithGivenTime(String time) throws ParseException {
         String date = getCurrentDate();
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         return format.format(format.parse(date+" "+time));
     }
 }
