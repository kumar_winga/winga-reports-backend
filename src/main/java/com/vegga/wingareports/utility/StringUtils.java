/**
 * Copyright (c) 2015, EightFolds its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 */

package com.vegga.wingareports.utility;

public class StringUtils {

	public static String maskMobileNumber(String mobile) {
		final String mask = "*******";
		mobile = mobile == null ? mask : mobile;
		final int lengthOfMobileNumber = mobile.length();
		if (lengthOfMobileNumber > 2) {
			final int maskLen = Math.min(Math.max(lengthOfMobileNumber / 2, 2), 6);
			final int start = (lengthOfMobileNumber - maskLen) / 2;
			return mobile.substring(0, start) + mask.substring(0, maskLen) + mobile.substring(start + maskLen);
		}
		return mobile;
	}
}
