package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "form_content_lable_values")
public class FormContentLableValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "fclvId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fclvId;

    @Column(name = "fclId", nullable = false)
    private Long fclId;

    @Column(name = "fcId", nullable = false)
    private Long fcId;

    @Column(name = "userId", nullable = false)
    private Long userId;

    @Column(name = "value")
    private String value;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "createdTime")
    private Date createdTime;

}
