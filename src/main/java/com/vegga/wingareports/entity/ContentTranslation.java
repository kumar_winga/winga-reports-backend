package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "content_translations")
public class ContentTranslation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "contentId")
    private Long contentId;

    @Column(name = "langId")
    private Long langId;

    /**
     * 1 for youtube video,2 for audio,3 for image
     */
    @Column(name = "type")
    private Integer type;

    @Column(name = "fileId")
    private Long fileId;

    @Column(name = "duration")
    private Long duration = 0L;

    /**
     * 0 for false, 1 for true
     */
    @Column(name = "sameAsEnglish")
    private Boolean sameAsEnglish = Boolean.FALSE;

    /**
     * 0 false, 1 true
     */
    @Column(name = "dirtyFlag")
    private Boolean dirtyFlag = Boolean.FALSE;

    /**
     * 0 for pending,-1 for rejected,1 for waiting for approval,2 for approved
     */
    @Column(name = "approveStatus")
    private Integer approveStatus = 0;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "modifiedBy")
    private Long modifiedBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "youtubeVid")
    private String youtubeVid;

}
