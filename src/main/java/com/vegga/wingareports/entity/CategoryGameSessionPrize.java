package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_session_prizes")
public class CategoryGameSessionPrize implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cgspId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cgspId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "cgsId")
    private Long cgsId;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    /**
     * 1:Item, 2: Money, 3: Addhoc Money, 4: Points
     */
    @Column(name = "type")
    private Integer type;

    @Column(name = "amount")
    private Double amount = 0D;

    @Column(name = "points")
    private Long points = 0L;

    @Column(name = "url")
    private String url;

    @Column(name = "totalQty")
    private Integer totalQty = 0;

    @Column(name = "prizeQty")
    private Integer prizeQty = 0;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
