package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "global_settings")
public class GlobalSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "noOfContentsEachGame")
    private Integer noOfContentsEachGame;

    @Column(name = "noOfQuestionsEachContent")
    private Integer noOfQuestionsEachContent;

    @Column(name = "noOfOptionsEachQuestion")
    private Integer noOfOptionsEachQuestion;

    @Column(name = "firstGameEnable")
    private Boolean firstGameEnable = Boolean.TRUE;

    @Column(name = "firstGameNoOfContent")
    private Integer firstGameNoOfContent;

    @Column(name = "firstGameNoOfQuestionsEachContent")
    private Integer firstGameNoOfQuestionsEachContent;

    @Column(name = "firstGameNoOfOptionsEachQuestion")
    private Integer firstGameNoOfOptionsEachQuestion;

    /**
     * 1. Amount 2. Points
     */
    @Column(name = "firstGameLoyaltyType")
    private Integer firstGameLoyaltyType;

    @Column(name = "firstGameWinAmt")
    private Double firstGameWinAmt;

    @Column(name = "firstGameLostAmt")
    private Double firstGameLostAmt;

    @Column(name = "firstGameWinPoints")
    private Integer firstGameWinPoints;

    @Column(name = "firstGameLostPoints")
    private Integer firstGameLostPoints;

    /**
     * 1. Amount 2. Points
     */
    @Column(name = "bonusReferralLoyaltyType")
    private Integer bonusReferralLoyaltyType;

    @Column(name = "bonusReferralAmt")
    private Double bonusReferralAmt;

    @Column(name = "bonusReferralPoints")
    private Integer bonusReferralPoints;

    @Column(name = "noOfGameWinForEligibility")
    private Integer noOfGameWinForEligibility;

    @Column(name = "pointsForEachGameWin")
    private Double pointsForEachGameWin;

    @Column(name = "pointsForEachGameLost")
    private Double pointsForEachGameLost;

    @Column(name = "revenuePercentageAsPrize")
    private Double revenuePercentageAsPrize;

    @Column(name = "rupesForEachPoint")
    private Double rupesForEachPoint;

    @Column(name = "installationRevenueAmt")
    private Double installationRevenueAmt = 0D;

    @Column(name = "viewCountRate")
    private Double viewCountRate = 0D;

    @Column(name = "winnersBlockTime")
    private Integer winnersBlockTime;

    @Column(name = "campaingExpiryNotifyBefore")
    private Integer campaingExpiryNotifyBefore;

    @Column(name = "campaingViewsNotifyBefore")
    private Integer campaingViewsNotifyBefore;

    @Column(name = "imageDefaultDuration")
    private Long imageDefaultDuration;

    @Column(name = "timeTakenToAnswerMax")
    private Long timeTakenToAnswerMax;

    @Column(name = "setupVersion")
    private Long setupVersion = 0L;

    @Column(name = "minimumRedemptionPoint")
    private Double minimumRedemptionPoint;

    @Column(name = "paytmTxnFee")
    private Double paytmTxnFee = 0D;

    @Column(name = "paytmGst")
    private Double paytmGst = 0D;

    @Column(name = "xoxoboxTxnFee")
    private Double xoxoboxTxnFee = 0D;

    @Column(name = "xoxoboxGst")
    private Double xoxoboxGst = 0D;

    @Column(name = "defaultNoofGamesPerUser")
    private Integer defaultNoofGamesPerUser;

    @Column(name = "spinAndWin")
    private Boolean spinAndWin = Boolean.FALSE;

    @Column(name = "spinAndWinMaxVal")
    private Integer spinAndWinMaxVal;

    @Column(name = "spinAndWinMinVal")
    private Integer spinAndWinMinVal;

    @Column(name = "googleAdMobMaxViewCount")
    private Integer googleAdMobMaxViewCount = 0;

    @Column(name = "googleAdMobPerViewPoints")
    private Integer googleAdMobPerViewPoints = 0;

    @Column(name = "googleAdMobViewThreshold")
    private Integer googleAdMobViewThreshold = 0;

    @Column(name = "appShareImage")
    private String appShareImage;

    @Column(name = "appShareText")
    private String appShareText;

    @Column(name = "referralText")
    private String referralText;

}
