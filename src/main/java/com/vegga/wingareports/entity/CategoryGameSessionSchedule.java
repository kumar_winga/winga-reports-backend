package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_session_schedule")
public class CategoryGameSessionSchedule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cgssId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cgssId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "priority")
    private Integer priority = 0;

    @Column(name = "startTime")
    private Time startTime;

    @Column(name = "endTime")
    private Time endTime;

    @Column(name = "winerPubTime")
    private Time winerPubTime;

    @Column(name = "timeZone")
    private String timeZone;

    @Column(name = "active")
    private Boolean active = Boolean.FALSE;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "modifiedBy")
    private Long modifiedBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
