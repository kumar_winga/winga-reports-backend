package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_countries")
public class MetaCountry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "sortname", nullable = false)
    private String sortname;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "phonecode", nullable = false)
    private Integer phonecode;

}
