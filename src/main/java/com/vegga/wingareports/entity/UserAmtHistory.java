package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_amt_history")
public class UserAmtHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "credit")
    private Double credit;

    @Column(name = "debit")
    private Double debit;

    @Column(name = "refId")
    private Long refId;

    /**
     * 1: first game, 2: game, 3: reference, 4: loyality point, 5: Google AdMob, 6: Winner Points
     */
    @Column(name = "refType")
    private Integer refType;

    @Column(name = "createdTime")
    private Date createdTime;

}
