package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_redemptions_to_be_processed")
public class UserRedemptionToBeProcessed implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "redTbProsId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long redTbProsId;

    @Column(name = "merchantOrderId")
    private String merchantOrderId;

    @Column(name = "disbursementId")
    private Long disbursementId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "refId")
    private Long refId;

    /**
     * 1: winner_special_prizes, 2: winers, 3: user_redeem_requests
     */
    @Column(name = "refType")
    private Integer refType;

    /**
     * 1: Installation Bonus, 2: Referral Bonus, 3: Loyalty, 4: Gift
     */
    @Column(name = "type")
    private Integer type;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "currencyCode")
    private String currencyCode = "INR";

    @Column(name = "payeePhoneNumber")
    private String payeePhoneNumber;

    @Column(name = "productId")
    private String productId;

    /**
     * 1: Paytm, 2: Xoxoday, 3: Physical
     */
    @Column(name = "paymentPlatform")
    private Integer paymentPlatform = 1;

    @Column(name = "paymentPlatformTransactionId")
    private String paymentPlatformTransactionId;

    /**
     * 0 Requested, 1 Processed, 2 Pending/In Transit, 3 Sucess/Delivered, -1 Failed, -2 Rejected, 99 ProcessInQueue
     */
    @Column(name = "status")
    private Integer status = 0;

    @Column(name = "newReqSubmited")
    private Boolean newReqSubmited = Boolean.FALSE;

    @Column(name = "inQueue")
    private Integer inQueue = 0;

    @Column(name = "inQueueTime")
    private Date inQueueTime;

    @Column(name = "taskId")
    private String taskId;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "paymentPlatformStatusCode")
    private String paymentPlatformStatusCode;

    @Column(name = "paymentPlatformStatusMsg")
    private String paymentPlatformStatusMsg;

    @Column(name = "statusMsg")
    private String statusMsg;

    @Column(name = "extras")
    private String extras;

}
