package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "content_providers")
public class ContentProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cpId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cpId;

    @Column(name = "cpNumber")
    private String cpNumber;

    @Column(name = "name")
    private String name;

    @Column(name = "contactPersonName")
    private String contactPersonName;

    @Column(name = "contactPersonEmail")
    private String contactPersonEmail;

    @Column(name = "contactPersonMobile")
    private String contactPersonMobile;

    @Column(name = "gstn")
    private String gstn;

    @Column(name = "hsnSacNo")
    private String hsnSacNo;

    @Column(name = "countryId")
    private Long countryId;

    @Column(name = "stateId")
    private Long stateId;

    @Column(name = "city")
    private String city;

    @Column(name = "pincode")
    private String pincode;

    @Column(name = "soldTo")
    private Integer soldTo;

    @Column(name = "shipTo")
    private Integer shipTo;

    @Column(name = "billTo")
    private Integer billTo;

    /**
     * 1 active ,0 inactive
     */
    @Column(name = "state")
    private Integer state = 1;

    @Column(name = "cpTypeId")
    private Long cpTypeId;

    @Column(name = "regionId")
    private Long regionId;

    /**
     * userId
     */
    @Column(name = "lastModifiedBy")
    private Long lastModifiedBy;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTimeByCms")
    private Date modifiedTimeByCms;

    @Column(name = "address")
    private String address;

}
