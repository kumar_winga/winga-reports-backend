package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_scratch_cards")
public class CategoryGameScratchCard implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cgscId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cgscId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "categoryGameId")
    private Long categoryGameId;

    @Column(name = "points")
    private Double points;

    /**
     * 1:used, 0:unused
     */
    @Column(name = "status")
    private Integer status = 0;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
