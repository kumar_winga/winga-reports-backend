package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_age_ranges")
public class MetaAgeRange implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ageRangeId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ageRangeId;

    @Column(name = "title")
    private String title;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "createdTime")
    private Date createdTime;

}
