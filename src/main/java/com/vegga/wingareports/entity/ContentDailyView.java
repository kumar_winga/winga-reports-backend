package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "content_daily_views")
public class ContentDailyView {

    @Id
    @Column(name = "cdvId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cdvId;
    @Column(name = "cgsId")
    private Long cgsId;
    @Column(name = "categoryId")
    private Long categoryId;
    @Column(name = "contentId")
    private Long contentId;
    @Column(name = "cpId")
    private Long cpId;
    @Column(name = "campaignId")
    private Long campaignId;
    @Column(name = "campaignGrpId")
    private Long campaignGrpId;
    @Column(name = "totalPlayDuration")
    private Long totalPlayDuration;
    @Column(name = "totalUniquePlayDuration")
    private Long totalUniquePlayDuration;
    @Column(name = "totalNoOfTimesVideoPlayed")
    private Long totalNoOfTimesVideoPlayed;
    @Column(name = "noOfUniqueUsersPlayed")
    private Long noOfUniqueUsersPlayed;
    @Column(name = "totalTimeTakenToAnswer")
    private Long totalTimeTakenToAnswer;
    @Column(name = "totalEngagementTime")
    private Long totalEngagementTime;
    @Column(name = "totalEngagementTimeActual")
    private Long totalEngagementTimeActual;
    @Column(name = "totalViewCount")
    private Long totalViewCount;
    @Column(name = "wingaLeads")
    private Long wingaLeads;
    @Column(name = "createdTime")
    private String createdTime;

}
