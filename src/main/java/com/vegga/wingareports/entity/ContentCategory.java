package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "content_categories")
public class ContentCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ContentCategoryPK contentCategoryPK;

}
