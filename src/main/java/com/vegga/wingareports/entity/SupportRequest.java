package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "support_requests")
public class SupportRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "supportReqId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long supportReqId;

    @Column(name = "supportNumber")
    private String supportNumber;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "supportTypeId")
    private Integer supportTypeId;

    /**
     * 0 for pending,-1 for rejected,1 for resolved
     */
    @Column(name = "state")
    private Integer state;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "message")
    private String message;

    @Column(name = "remark")
    private String remark;

}
