package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "questions")
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "qId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long qId;

    @Column(name = "contentId")
    private Long contentId;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "dirtyFlag")
    private Integer dirtyFlag = 1;

    /**
     * 0 for open,-1 for rejected, 1 for inprogress,2 for approval pending, 3 for approved
     */
    @Column(name = "approveStatus")
    private Integer approveStatus = 0;

    @Column(name = "title")
    private String title;

}
