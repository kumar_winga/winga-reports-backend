package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_support_types")
public class MetaSupportType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "supportTypeId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long supportTypeId;

    @Column(name = "title")
    private String title;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modfiedTime")
    private Date modfiedTime;

}
