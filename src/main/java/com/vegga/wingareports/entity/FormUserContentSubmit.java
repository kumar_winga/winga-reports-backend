package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "form_user_content_submit")
public class FormUserContentSubmit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "fucsId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fucsId;

    @Column(name = "userId", nullable = false)
    private Long userId;

    @Column(name = "contentId", nullable = false)
    private Long contentId;

    @Column(name = "categoryId", nullable = false)
    private Long categoryId;

    @Column(name = "fcId", nullable = false)
    private Long fcId;

    @Column(name = "verified")
    private Boolean verified;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "createdTime")
    private Date createdTime;

}
