package com.vegga.wingareports.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class UserconnectionPK implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userId;
    private String providerId;
    private String providerUserId = "";
}
