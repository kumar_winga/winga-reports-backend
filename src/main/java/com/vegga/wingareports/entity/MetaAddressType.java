package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_address_type")
public class MetaAddressType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "adreTypeId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long adreTypeId;

    @Column(name = "title")
    private String title;

    @Column(name = "deleted")
    private Integer deleted;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
