package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "footer_ads")
public class FooterAd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "footerAddId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long footerAddId;

    @Column(name = "title")
    private String title;

    /**
     * 1: url, 2: file
     */
    @Column(name = "type")
    private Integer type = 1;

    @Column(name = "fileId")
    private Long fileId;

    @Column(name = "viewCount")
    private Long viewCount = 0L;

    @Column(name = "startTime")
    private Date startTime;

    @Column(name = "endTime")
    private Date endTime;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "redirectLink")
    private String redirectLink;

    @Column(name = "frequency")
    private Long frequency;

    @Column(name = "url")
    private String url;

    @Column(name = "clickUrl")
    private String clickUrl;

}
