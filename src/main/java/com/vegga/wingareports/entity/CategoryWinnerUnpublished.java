package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_winners_unpublished")
public class CategoryWinnerUnpublished implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cwuId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cwuId;

    @Column(name = "cgsId")
    private Long cgsId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "cgspId")
    private Long cgspId;

    /**
     * 0 Not Processed, 1 Processed, 2 Pending/In Transit, 3 Sucess/Delivered, -1 Failed, -2 Rejected
     */
    @Column(name = "state")
    private Integer state = 0;

    @Column(name = "gameDate")
    private Date gameDate;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "stateMsg")
    private String stateMsg;

}
