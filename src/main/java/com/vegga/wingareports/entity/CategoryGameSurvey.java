package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_survey")
public class CategoryGameSurvey implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ccqsId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ccqsId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "contentId")
    private Long contentId;

    @Column(name = "questionId")
    private Long questionId;

    @Column(name = "optionId")
    private Long optionId;

    @Column(name = "comment")
    private String comment;

    @Column(name = "createdTime")
    private Date createdTime;

}
