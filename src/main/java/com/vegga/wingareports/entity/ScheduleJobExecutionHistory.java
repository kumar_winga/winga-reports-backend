package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "schedule_jobe_execution_history")
public class ScheduleJobExecutionHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jobId")
    private Long jobId;

    @Column(name = "jobType")
    private String jobType;

    @Column(name = "executionStart")
    private Date executionStart;

    @Column(name = "executionEnd")
    private Date executionEnd;

    @Column(name = "jobInstanceHost")
    private String jobInstanceHost;

    @Column(name = "state")
    private Integer state;

    @Column(name = "details")
    private String details;

}
