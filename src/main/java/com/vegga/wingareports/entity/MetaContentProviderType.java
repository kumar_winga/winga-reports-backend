package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_content_provider_types")
public class MetaContentProviderType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cpTypeId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cpTypeId;

    @Column(name = "name")
    private String name;

}
