package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_results")
public class CategoryGameResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cgrId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cgrId;

    @Column(name = "categoryGameId")
    private Long categoryGameId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "contentId")
    private Long contentId;

    @Column(name = "qId")
    private Long qId;

    @Column(name = "ansOppId")
    private Long ansOppId;

    @Column(name = "ansCorrect")
    private Integer ansCorrect;

    @Column(name = "isoCode")
    private String isoCode;

    @Column(name = "createdTime")
    private Date createdTime;

}
