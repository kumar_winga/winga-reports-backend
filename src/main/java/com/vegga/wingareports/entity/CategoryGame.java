package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_games")
public class CategoryGame implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "categoryGameId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long categoryGameId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "cgsId")
    private Long cgsId;

    @Column(name = "firstGame")
    private Boolean firstGame = Boolean.FALSE;

    @Column(name = "userPrefStateId")
    private Long userPrefStateId;

    @Column(name = "state")
    private Integer state = 0;

    @Column(name = "gameWin")
    private Boolean gameWin = Boolean.FALSE;

    @Column(name = "submitTime")
    private Date submitTime;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
