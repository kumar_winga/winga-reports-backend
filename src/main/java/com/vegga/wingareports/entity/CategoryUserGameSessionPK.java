package com.vegga.wingareports.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.sql.Date;

@Embeddable
@Data
public class CategoryUserGameSessionPK implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long userId;
    private Long cgsId;
    private Long categoryId;
    private Date createdDate;
}
