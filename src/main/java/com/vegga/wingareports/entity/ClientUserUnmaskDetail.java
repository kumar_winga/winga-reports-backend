package com.vegga.wingareports.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "client_user_unmask_details")
@DynamicUpdate
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientUserUnmaskDetail implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long cuudId;
    private Long clientUserId;
    private Long cpId;
    private String username;
    private String fromDate;
    private String toDate;
    private Integer unMaskCount;
    private String createdTime;
    private String modifiedTime;

}
