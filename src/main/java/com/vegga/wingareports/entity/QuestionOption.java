package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "question_options")
public class QuestionOption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "oppId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oppId;

    @Column(name = "qId")
    private Long qId;

    @Column(name = "correct")
    private Boolean correct = Boolean.FALSE;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "dirtyFlag")
    private Integer dirtyFlag = 0;

}
