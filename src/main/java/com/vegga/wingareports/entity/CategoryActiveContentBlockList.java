package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_active_content_block_list")
public class CategoryActiveContentBlockList implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CategoryActiveContentBlockListPK categoryActiveContentBlockListPK;

    @Column(name = "playCnt")
    private Integer playCnt = 1;

    @Column(name = "ttlPlayCnt")
    private Integer ttlPlayCnt = 0;

}
