package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_winner_block_list")
public class CategoryWinnerBlockList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cwblId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cwblId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "createdTime")
    private Date createdTime;

}
