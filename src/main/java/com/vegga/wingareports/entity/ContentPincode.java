package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "content_pincodes")
public class ContentPincode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "contentId", nullable = false)
    private Long contentId;

    @Column(name = "pinCode")
    private String pinCode;

}
