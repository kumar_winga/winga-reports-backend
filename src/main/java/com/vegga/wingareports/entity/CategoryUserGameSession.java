package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_user_game_sessions")
public class CategoryUserGameSession implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CategoryUserGameSessionPK categoryUserGameSessionPK;

    @Column(name = "noOfGamePlayed")
    private Integer noOfGamePlayed = 0;

    @Column(name = "noOfWins")
    private Integer noOfWins = 0;

    @Column(name = "pointsWin")
    private Integer pointsWin = 0;

    @Column(name = "eligible")
    private Integer eligible = 0;

    @Column(name = "drawWin")
    private Integer drawWin = 0;

    @Column(name = "allowedGamesPerUser")
    private Integer allowedGamesPerUser;

}
