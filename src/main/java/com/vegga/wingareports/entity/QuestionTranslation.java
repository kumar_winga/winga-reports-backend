package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "question_translations")
public class QuestionTranslation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "qId")
    private Long qId;

    @Column(name = "langId")
    private Long langId;

    @Column(name = "dirtyFlag")
    private Integer dirtyFlag = 0;

    @Column(name = "translatedBy")
    private Long translatedBy;

    /**
     * 0 for open,-1 for rejected, 1 for inprogress(Draft),2 for approval pending, 3 for approved
     */
    @Column(name = "approveStatus")
    private Integer approveStatus = 0;

    @Column(name = "txt")
    private String txt;

}
