package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_redeem_requests")
public class UserRedeemRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "redeemReqId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long redeemReqId;

    @Column(name = "redeemReqGroupId")
    private String redeemReqGroupId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "points")
    private Double points;

    /**
     * denomination
     */
    @Column(name = "amt")
    private Double amt;

    @Column(name = "payTmNoReq")
    private String payTmNoReq;

    @Column(name = "productId")
    private String productId;

    @Column(name = "quantity")
    private Integer quantity;

    /**
     * 1 Paytm, 2 Xoxoday
     */
    @Column(name = "paymentPlatform")
    private Integer paymentPlatform;

    /**
     * 0 Requested, 1 Processed, 2 Pending, 3 Sucess, -1 Failed, -2 Rejected
     */
    @Column(name = "status")
    private Integer status;

    @Column(name = "processedTime")
    private Date processedTime;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "reversedTime")
    private Date reversedTime;

    @Column(name = "processedPayTmNo")
    private String processedPayTmNo;

}
