package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "mail_archive")
public class MailArchive implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "serviceId")
    private Long serviceId;

    @Column(name = "to")
    private String to;

    @Column(name = "subject")
    private String subject;

    @Column(name = "attachmentPaths")
    private String attachmentPaths;

    /**
     * 0=false, 1=true
     */
    @Column(name = "html")
    private Boolean html = Boolean.FALSE;

    /**
     * -1=failed, 2=success
     */
    @Column(name = "state")
    private Boolean state = Boolean.FALSE;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "body")
    private String body;

}
