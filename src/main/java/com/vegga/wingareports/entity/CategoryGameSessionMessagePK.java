package com.vegga.wingareports.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class CategoryGameSessionMessagePK implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long categoryId;
    private Long code;
    private Long langId;
}
