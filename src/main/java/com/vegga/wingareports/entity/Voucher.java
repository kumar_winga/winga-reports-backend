package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "vouchers")
public class Voucher implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_id", nullable = false)
    private String productId;

    @Column(name = "product_name", nullable = false)
    private String productName;

    @Column(name = "imageId")
    private Long imageId;

    @Column(name = "image")
    private String image;

    @Column(name = "denomination")
    private Double denomination;

    @Column(name = "currency_code")
    private String currencyCode;

    @Column(name = "currency_name")
    private String currencyName;

    @Column(name = "country")
    private String country;

    @Column(name = "conversion_ratio")
    private String conversionRatio;

    /**
     * Product with Customized Denomination. If '1' yes, else no
     */
    @Column(name = "is_variable")
    private Boolean variable;

    @Column(name = "max_price")
    private Double maxPrice;

    @Column(name = "min_price")
    private Double minPrice;

    /**
     * 1 for active ,, 0 for inactive
     */
    @Column(name = "active", nullable = false)
    private Integer active = 0;

    @Column(name = "createDate")
    private Date createDate;

    @Column(name = "modifiedDate")
    private Date modifiedDate;

    @Column(name = "description")
    private String description;

    @Column(name = "terms_and_condition")
    private String termsAndCondition;

    @Column(name = "validity")
    private String validity;

    @Column(name = "how_to_use")
    private String howToUse;

    @Column(name = "raw_response")
    private String rawResponse;

}
