package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "campaign_groups")
public class CampaignGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "campaignGroupId", nullable = false)
    private Long campaignGroupId;

    @Column(name = "campaignGroupNumber")
    private String campaignGroupNumber;

    @Column(name = "cpId")
    private Long cpId;

    @Column(name = "title")
    private String title;

    /**
     * 1 active ,0 inactive
     */
    @Column(name = "status")
    private Integer status = 1;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "modifiedBy")
    private Long modifiedBy = 0L;

    @Column(name = "desc")
    private String desc;

}
