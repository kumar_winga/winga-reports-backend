package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_contents")
public class CategoryGameContent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cgcId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cgcId;

    @Column(name = "categoryGameId")
    private Long categoryGameId;

    @Column(name = "contentId")
    private Long contentId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "playDuration")
    private Long playDuration = 0L;

    @Column(name = "uniquePlayDuration")
    private Long uniquePlayDuration = 0L;

    @Column(name = "noOfTimesVideoPlayed")
    private Integer noOfTimesVideoPlayed = 0;

    @Column(name = "timeTakenToAnswer")
    private Long timeTakenToAnswer = 0L;

    @Column(name = "totalEngagementTime")
    private Long totalEngagementTime = 0L;

    @Column(name = "totalEngagementTimeActual")
    private Long totalEngagementTimeActual;

    @Column(name = "outlier")
    private Boolean outlier = Boolean.FALSE;

    @Column(name = "viewCount")
    private Long viewCount = 0L;

    @Column(name = "viewCountRate")
    private Integer viewCountRate;

    @Column(name = "revenueAmt")
    private Double revenueAmt = 0D;

    @Column(name = "amtGivenAsLoyality")
    private Double amtGivenAsLoyality = 0D;

    @Column(name = "submitted")
    private Boolean submitted = Boolean.FALSE;

    @Column(name = "processed")
    private Boolean processed = Boolean.FALSE;

    @Column(name = "revenuRealized")
    private Boolean revenuRealized = Boolean.FALSE;

}
