package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "contents_buy_link_logs")
public class ContentBuyLinkLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "logId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long logId;

    @Column(name = "contentId", nullable = false)
    private Long contentId;

    @Column(name = "userId", nullable = false)
    private Long userId;

    @Column(name = "createdTime")
    private Date createdTime;

}
