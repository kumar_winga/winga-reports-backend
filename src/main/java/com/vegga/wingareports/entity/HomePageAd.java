package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "home_page_ads")
public class HomePageAd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "homePageAddId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long homePageAddId;

    @Column(name = "title")
    private String title;

    /**
     * 1: url, 2: file
     */
    @Column(name = "type")
    private Integer type = 1;

    @Column(name = "fileId")
    private Long fileId;

    @Column(name = "viewCount")
    private Long viewCount = 0L;

    @Column(name = "startTime")
    private Date startTime;

    @Column(name = "endTime")
    private Date endTime;

    /**
     * 1 active , 0 inactive
     */
    @Column(name = "status")
    private Integer status = 1;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "redirectLink")
    private String redirectLink;

    @Column(name = "youtubeId")
    private String youtubeId;

    @Column(name = "clickUrl")
    private String clickUrl;

}
