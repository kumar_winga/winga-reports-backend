package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_active_contents")
public class CategoryActiveContent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cacId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cacId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "contentId")
    private Long contentId;

    @Column(name = "campaignId")
    private Long campaignId;

    @Column(name = "cpId")
    private Long cpId;

    @Column(name = "expiry")
    private Date expiry;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "geoLocked")
    private Boolean geoLocked = Boolean.FALSE;

    @Column(name = "type")
    private Integer type;

    @Column(name = "suitableForMinors")
    private Boolean suitableForMinors = Boolean.TRUE;

    @Column(name = "bannerDuration")
    private Long bannerDuration;

    /**
     * 0 both, 1:male, 2:female
     */
    @Column(name = "gender")
    private Integer gender = 0;

    @Column(name = "suitableForChildren")
    private Integer suitableForChildren = 1;

    @Column(name = "form")
    private Boolean form = Boolean.FALSE;

    @Column(name = "survey")
    private Boolean survey = Boolean.FALSE;

}
