package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_active_content_locations")
public class CategoryActiveContentLocation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "caclId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long caclId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "contentId")
    private Long contentId;

    @Column(name = "stateId")
    private Long stateId;

    @Column(name = "districtId")
    private Long districtId;

    @Column(name = "talukId")
    private Long talukId;

    @Column(name = "pincodes")
    private String pincodes;

}
