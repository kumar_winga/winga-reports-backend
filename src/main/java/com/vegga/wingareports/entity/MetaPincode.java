package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_pincodes")
public class MetaPincode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "pinCodeId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pinCodeId;

    @Column(name = "pinCode")
    private String pinCode;

    @Column(name = "postOffice")
    private String postOffice;

    @Column(name = "talukId")
    private Long talukId;

    @Column(name = "districtId")
    private Long districtId;

    @Column(name = "stateId")
    private Long stateId;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
