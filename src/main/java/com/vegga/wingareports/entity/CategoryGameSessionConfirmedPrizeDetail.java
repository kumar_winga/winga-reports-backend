package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_session_confirmed_prize_details")
public class CategoryGameSessionConfirmedPrizeDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cgscpdId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cgscpdId;

    @Column(name = "cgscpmId")
    private Long cgscpmId;

    @Column(name = "cgspId")
    private Long cgspId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "qty")
    private Integer qty = 0;

    @Column(name = "amtPerQty")
    private Double amtPerQty = 0D;

    @Column(name = "amtTotal")
    private Double amtTotal = 0D;

    @Column(name = "pointsPerQty")
    private Long pointsPerQty = 0L;

    @Column(name = "pointsTotal")
    private Long pointsTotal = 0L;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
