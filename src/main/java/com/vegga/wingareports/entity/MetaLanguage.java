package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_languages")
public class MetaLanguage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "langId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long langId;

    @Column(name = "isoCode")
    private String isoCode;

    @Column(name = "title")
    private String title;

    @Column(name = "dispTitle")
    private String dispTitle;

    @Column(name = "priority")
    private Integer priority = 1;

    @Column(name = "defaultLang")
    private Boolean defaultLang = Boolean.FALSE;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
