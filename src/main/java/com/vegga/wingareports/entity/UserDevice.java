package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_devices")
public class UserDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "deviceId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "platformId")
    private Integer platformId;

    @Column(name = "pushRegId")
    private String pushRegId;

    @Column(name = "imei")
    private String imei;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
