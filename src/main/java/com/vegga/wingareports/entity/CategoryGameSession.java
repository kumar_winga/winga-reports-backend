package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_sessions")
public class CategoryGameSession implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cgsId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cgsId;

    @Column(name = "cgssId")
    private Long cgssId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "startTime")
    private Date startTime;

    @Column(name = "endTime")
    private Date endTime;

    @Column(name = "noOfContentsEachGame")
    private Integer noOfContentsEachGame = 0;

    @Column(name = "noOfQuestionsEachContent")
    private Integer noOfQuestionsEachContent = 0;

    @Column(name = "noOfOptionsEachQuestion")
    private Integer noOfOptionsEachQuestion = 0;

    @Column(name = "noOfGameWinForEligibility")
    private Integer noOfGameWinForEligibility = 0;

    @Column(name = "pointsForEachGameWin")
    private Integer pointsForEachGameWin = 0;

    @Column(name = "pointsForEachGameLost")
    private Integer pointsForEachGameLost = 0;

    /**
     * 0: Open, 1:Close, 2: Selecting Winers, 3: Winer published, -1: Cancled, -2: Failed
     */
    @Column(name = "state")
    private Integer state = 0;

    @Column(name = "revenuePercentageAsPrize")
    private Float revenuePercentageAsPrize;

    @Column(name = "totalEligibleCandidates")
    private Integer totalEligibleCandidates = 0;

    @Column(name = "totalWiners")
    private Integer totalWiners = 0;

    @Column(name = "winerPubTime")
    private Date winerPubTime;

    @Column(name = "paused")
    private Boolean paused = Boolean.FALSE;

    @Column(name = "lastPausedBy")
    private Long lastPausedBy;

    @Column(name = "lastResumedBy")
    private Long lastResumedBy;

    @Column(name = "sponsored")
    private Boolean sponsored = Boolean.FALSE;

    @Column(name = "sponsoredCampaignId")
    private Long sponsoredCampaignId;

    @Column(name = "sessionMessage")
    private String sessionMessage;

    @Column(name = "showAnswer")
    private String showAnswer;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "deletedBy")
    private Long deletedBy;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "modifiedBy")
    private Long modifiedBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
