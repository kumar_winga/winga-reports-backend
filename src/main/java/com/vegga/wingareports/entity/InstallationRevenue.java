package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "installation_revenues")
public class InstallationRevenue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "irId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long irId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "initailAmt")
    private Double initailAmt = 0D;

    @Column(name = "givenBonusAmt")
    private Double givenBonusAmt = 0D;

    @Column(name = "finalAmt")
    private Double finalAmt = 0D;

    /**
     * 0: Waiting FG, 1: Pending, 2: Processing, 3: Processed
     */
    @Column(name = "state")
    private Integer state = 0;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
