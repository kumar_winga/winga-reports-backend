package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "winner_special_prizes")
public class WinnerSpecialPrize implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "winnerOtherId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long winnerOtherId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "title")
    private String title;

    /**
     * 1:item, 2: Money, 3: Points
     */
    @Column(name = "type")
    private Integer type;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "points")
    private Integer points;

    /**
     * 0 Not Processed, 1 Processed, 2 Pending/In Transit, 3 Sucess/Delivered, -1 Failed, -2 Rejected, -10 Not Requested
     */
    @Column(name = "state")
    private Integer state = 0;

    @Column(name = "refId")
    private Long refId;

    /**
     * 1: loyality points, 2: referal, 3: instal bonus
     */
    @Column(name = "refType")
    private Integer refType;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "desc")
    private String desc;

    @Column(name = "stateMsg")
    private String stateMsg;

}
