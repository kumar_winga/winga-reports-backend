package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_redemption_options")
public class MetaRedemptionOption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "subTitle")
    private String subTitle;

    /**
     * 0 for false,1 for true
     */
    @Column(name = "enabled")
    private Boolean enabled = Boolean.TRUE;

    @Column(name = "createdTime")
    private Date createdTime;

}
