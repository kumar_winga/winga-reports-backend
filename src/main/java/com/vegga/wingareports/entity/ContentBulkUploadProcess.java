package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "content_bulk_upload_process")
public class ContentBulkUploadProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "processId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long processId;

    @Column(name = "uploadedBy")
    private Long uploadedBy;

    /**
     * 1 for master,2 for translation
     */
    @Column(name = "type")
    private Integer type;

    @Column(name = "excelFileName")
    private String excelFileName;

    @Column(name = "progress")
    private Float progress;

    /**
     * 0 for pending,1 for processing,2 for finished,-1 for failed
     */
    @Column(name = "status")
    private Integer status;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
