package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "categories")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "categoryId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long categoryId;

    @Column(name = "categoryName", nullable = false)
    private String categoryName;

    @Column(name = "active")
    private Boolean active = Boolean.FALSE;

    @Column(name = "noOfContentsEachGame")
    private Integer noOfContentsEachGame;

    @Column(name = "noOfQuestionsEachContent")
    private Integer noOfQuestionsEachContent;

    @Column(name = "noOfOptionsEachQuestion")
    private Integer noOfOptionsEachQuestion;

    @Column(name = "noOfGameWinForEligibility")
    private Integer noOfGameWinForEligibility;

    @Column(name = "pointsForEachGameWin")
    private Integer pointsForEachGameWin;

    @Column(name = "pointsForEachGameLost")
    private Integer pointsForEachGameLost;

    @Column(name = "revenuePercentageAsPrize")
    private Integer revenuePercentageAsPrize;

    @Column(name = "defaultNoofGamesPerUser")
    private Integer defaultNoofGamesPerUser;

    @Column(name = "spinAndWin")
    private Boolean spinAndWin = Boolean.FALSE;

    @Column(name = "spinAndWinMinVal")
    private Integer spinAndWinMinVal;

    @Column(name = "spinAndWinMaxVal")
    private Integer spinAndWinMaxVal;

    /**
     * 1. Regular Based 2. Daily Based
     */
    @Column(name = "categoryType")
    private Integer categoryType;

    @Column(name = "showAnswer")
    private Boolean showAnswer;

    @Column(name = "shortSession")
    private Boolean shortSession;

    @Column(name = "googleAdMobMaxViewCount")
    private Integer googleAdMobMaxViewCount = 0;

    @Column(name = "googleAdMobPerViewPoints")
    private Integer googleAdMobPerViewPoints = 0;

    @Column(name = "googleAdMobViewThreshold")
    private Integer googleAdMobViewThreshold = 0;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "modifiedBy")
    private Long modifiedBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
