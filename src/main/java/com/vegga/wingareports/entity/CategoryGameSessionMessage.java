package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_session_messages")
public class CategoryGameSessionMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private CategoryGameSessionMessagePK categoryGameSessionMessagePK;

    @Column(name = "message")
    private String message;

}
