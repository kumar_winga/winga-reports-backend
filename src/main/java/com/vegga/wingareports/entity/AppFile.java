package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "app_files")
public class AppFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "fileId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fileId;

    @Column(name = "serviceId")
    private Integer serviceId;

    @Column(name = "mimeType")
    private String mimeType;

    @Column(name = "fileName")
    private String fileName;

    @Column(name = "fileStorageType")
    private Integer fileStorageType = 1;

    @Column(name = "filePath")
    private String filePath;

    @Column(name = "fileSize")
    private Long fileSize;

    @Column(name = "checksum")
    private String checksum;

    @Column(name = "width")
    private String width;

    @Column(name = "height")
    private String height;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
