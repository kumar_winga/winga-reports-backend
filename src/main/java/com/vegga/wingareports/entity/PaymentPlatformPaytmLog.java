package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "payment_platform_paytm_log")
public class PaymentPlatformPaytmLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "redTbProsId")
    private Long redTbProsId;

    @Column(name = "taskId")
    private String taskId;

    @Column(name = "payeePhoneNumber")
    private String payeePhoneNumber;

    @Column(name = "time")
    private Long time;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "paymentPlatformUrl")
    private String paymentPlatformUrl;

    @Column(name = "paymentPlatformHeaders")
    private String paymentPlatformHeaders;

    @Column(name = "paymentPlatformReq")
    private String paymentPlatformReq;

    @Column(name = "paymentPlatformRes")
    private String paymentPlatformRes;

}
