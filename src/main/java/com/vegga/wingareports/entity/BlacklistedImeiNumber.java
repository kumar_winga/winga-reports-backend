package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "blacklisted_imei_numbers")
public class BlacklistedImeiNumber implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "imei", nullable = false)
    private String imei;

    @Column(name = "blacklistTime")
    private Date blacklistTime;

}
