package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_google_adds")
public class UserGoogleAdd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ugaId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ugaId;

    @Column(name = "userId", nullable = false)
    private Long userId;

    @Column(name = "gSessionId")
    private Long gSessionId;

    @Column(name = "viewCount")
    private Integer viewCount = 0;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
