package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "schedule_jobs")
public class ScheduleJob implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "jobId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long jobId;

    @Column(name = "jobType")
    private String jobType;

    @Column(name = "jobUniqueNo")
    private String jobUniqueNo;

    @Column(name = "executeAfter")
    private Date executeAfter;

    /**
     * 0: pending, 1: in-progress, 2: completed, -1: failed, -2: cancled
     */
    @Column(name = "state")
    private Integer state = 0;

    @Column(name = "jobInstanceHost")
    private String jobInstanceHost;

    @Column(name = "refId")
    private Long refId;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "details")
    private String details;

}
