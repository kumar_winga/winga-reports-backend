package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "users")
@Accessors(chain = true)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "userId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @Column(name = "userNumber")
    private String userNumber;

    @Column(name = "serviceId")
    private Integer serviceId;

    @Column(name = "name")
    private String name;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "profilePicId")
    private Long profilePicId;

    @Column(name = "locked")
    private Boolean locked = Boolean.FALSE;

    /**
     * 1 true 0 false
     */
    @Column(name = "enabled")
    private Boolean enabled = Boolean.TRUE;

    /**
     * 1 true 0 false
     */
    @Column(name = "expired")
    private Boolean expired = Boolean.FALSE;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "countryId")
    private Long countryId;

    @Column(name = "stateId")
    private Long stateId;

    @Column(name = "districtId")
    private Long districtId;

    @Column(name = "talukId")
    private Long talukId;

    @Column(name = "city")
    private String city;

    @Column(name = "pincode")
    private String pincode;

    @Column(name = "totalLoyalityPoints")
    private Integer totalLoyalityPoints = 0;

    @Column(name = "redeemedLoyalityPoints")
    private Integer redeemedLoyalityPoints = 0;

    @Column(name = "currentLevel")
    private Long currentLevel = 1L;

    @Column(name = "redeemableAmt")
    private Double redeemableAmt = 0D;

    @Column(name = "redeemedAmt")
    private Double redeemedAmt = 0D;

    @Column(name = "totalGamePlayed")
    private Integer totalGamePlayed = 0;

    @Column(name = "totalWins")
    private Integer totalWins = 0;

    @Column(name = "ageRangeId")
    private Long ageRangeId = 0L;

    @Column(name = "referralCode")
    private String referralCode;

    @Column(name = "referedBy")
    private Long referedBy;

    /**
     * 1 true 0 false
     */
    @Column(name = "mobileVerified")
    private Boolean mobileVerified = Boolean.FALSE;

    /**
     * 1 true 0 false
     */
    @Column(name = "notification")
    private Boolean notification = Boolean.TRUE;

    /**
     * 1 true 0 false
     */
    @Column(name = "emailVerified")
    private Boolean emailVerified = Boolean.FALSE;

    @Column(name = "alert")
    private Integer alert = 0;

    @Column(name = "promotion")
    private Integer promotion = 0;

    @Column(name = "updates")
    private Integer updates = 0;

    @Column(name = "preferredStateId")
    private Long preferredStateId;

    @Column(name = "firstGamePlayed")
    private Integer firstGamePlayed = 0;

    @Column(name = "countryCode")
    private String countryCode;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "paytmNo")
    private String paytmNo;

    /**
     * 0 both, 1 audio,video ,2 banner
     */
    @Column(name = "preferredContentType")
    private Integer preferredContentType = 0;

    /**
     * 1 male , 2 female
     */
    @Column(name = "gender")
    private Integer gender;

    @Column(name = "imei")
    private String imei;

    @Column(name = "installationId")
    private String installationId;

}
