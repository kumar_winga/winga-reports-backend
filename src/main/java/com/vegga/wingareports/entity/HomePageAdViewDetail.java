package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "home_page_ad_view_details")
public class HomePageAdViewDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "homePageAddId")
    private Long homePageAddId;

    @Column(name = "playDuration")
    private Long playDuration = 0L;

    @Column(name = "uniquePlayDuration")
    private Long uniquePlayDuration = 0L;

    @Column(name = "noOfTimesVideoPlayed")
    private Integer noOfTimesVideoPlayed = 0;

    @Column(name = "timeTakenToAnswer")
    private Long timeTakenToAnswer = 0L;

    @Column(name = "totalEngagementTime")
    private Long totalEngagementTime = 0L;

    @Column(name = "totalEngagementTimeActual")
    private Long totalEngagementTimeActual;

    @Column(name = "outlier")
    private Boolean outlier = Boolean.FALSE;

    @Column(name = "viewCount")
    private Long viewCount = 0L;

    @Column(name = "viewCountRate")
    private Integer viewCountRate;

    @Column(name = "revenueAmt")
    private Double revenueAmt = 0D;

    @Column(name = "amtGivenAsLoyality")
    private Double amtGivenAsLoyality = 0D;

    @Column(name = "submitted")
    private Boolean submitted = Boolean.FALSE;

    @Column(name = "processed")
    private Boolean processed = Boolean.FALSE;

    @Column(name = "revenuRealized")
    private Boolean revenuRealized = Boolean.FALSE;

    @Column(name = "platformId")
    private Integer platformId;

    @Column(name = "imei")
    private String imei;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "appViewDateTime")
    private Date appViewDateTime;

    @Column(name = "stateId")
    private Long stateId;

}
