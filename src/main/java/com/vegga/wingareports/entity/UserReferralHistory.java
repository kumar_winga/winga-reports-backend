package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_referral_history")
public class UserReferralHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "reffredUserId")
    private Long reffredUserId;

    @Column(name = "earnedAmt")
    private Double earnedAmt;

    @Column(name = "earnedPoint")
    private Integer earnedPoint;

    @Column(name = "createdTime")
    private Date createdTime;

}
