package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_taluks")
public class MetaTaluk implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "talukId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long talukId;

    @Column(name = "stateId")
    private Long stateId;

    @Column(name = "districtId")
    private Long districtId;

    @Column(name = "name")
    private String name;

    @Column(name = "deleted")
    private Integer deleted;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
