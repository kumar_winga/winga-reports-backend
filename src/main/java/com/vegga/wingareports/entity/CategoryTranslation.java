package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_translations")
public class CategoryTranslation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ctId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ctId;

    @Column(name = "categoryId", nullable = false)
    private Long categoryId;

    @Column(name = "langId", nullable = false)
    private Integer langId;

    @Column(name = "categoryName")
    private String categoryName;

    @Column(name = "active")
    private Boolean active = Boolean.FALSE;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "modifiedBy")
    private Long modifiedBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
