package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "form_content_lables")
public class FormContentLable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "fclId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fclId;

    @Column(name = "fcId", nullable = false)
    private Long fcId;

    @Column(name = "fftId", nullable = false)
    private Integer fftId;

    @Column(name = "lableName", nullable = false)
    private String lableName;

    @Column(name = "lableValue")
    private String lableValue;

    @Column(name = "defaultValue")
    private String defaultValue;

    @Column(name = "regex")
    private String regex;

    @Column(name = "modifiedBy", nullable = false)
    private Long modifiedBy;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "createdTime")
    private Date createdTime;

}
