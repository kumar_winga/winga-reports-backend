package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_coupon_winners")
public class CategoryCouponWinner implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "categoryCouponWinnerId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long categoryCouponWinnerId;

    @Column(name = "cgsId", nullable = false)
    private Long cgsId;

    @Column(name = "cgspId")
    private Long cgspId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "cecId")
    private Long cecId;

    @Column(name = "couponId")
    private Long couponId;

    @Column(name = "smsArchiveId")
    private Long smsArchiveId;

    @Column(name = "state")
    private Integer state;

    @Column(name = "gameDate")
    private String gameDate;

    @Column(name = "processedBy")
    private Long processedBy;

    @Column(name = "createdTime")
    private String createdTime;

    @Column(name = "modifiedTime")
    private String modifiedTime;

}
