package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "campaigns")
public class Campaign implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "campaignId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long campaignId;

    @Column(name = "campaignNumber")
    private String campaignNumber;

    @Column(name = "cpId")
    private Long cpId;

    @Column(name = "title")
    private String title;

    @Column(name = "startDateTime")
    private Date startDateTime;

    @Column(name = "endDateTime")
    private Date endDateTime;

    @Column(name = "targetViewCount")
    private Integer targetViewCount;

    @Column(name = "consumedViewCount")
    private Integer consumedViewCount = 0;

    @Column(name = "dailyViewCount")
    private Integer dailyViewCount = 0;

    @Column(name = "gameWinFixedPoint")
    private Integer gameWinFixedPoint;

    @Column(name = "gameLossFixedPoint")
    private Integer gameLossFixedPoint;

    @Column(name = "costPerView")
    private Double costPerView;

    @Column(name = "totalAmount")
    private Double totalAmount;

    @Column(name = "priority")
    private Integer priority;

    /**
     * 0: Draft, 1: Approved, 2: Active, 3: Inactive, 4: Completed, -1: Cancelled
     */
    @Column(name = "state")
    private Integer state;

    @Column(name = "geoLocked")
    private Boolean geoLocked = Boolean.FALSE;

    @Column(name = "sponsored")
    private Boolean sponsored = Boolean.FALSE;

    @Column(name = "lastModifiedBy")
    private Long lastModifiedBy;

    /**
     * 0 for in progress,1 for completed
     */
    @Column(name = "cloneStatus")
    private Integer cloneStatus;

    /**
     * camapingId
     */
    @Column(name = "copiedFrom")
    private Long copiedFrom;

    @Column(name = "campaignGrpId")
    private Long campaignGrpId;

    @Column(name = "dailyViewLimit")
    private Long dailyViewLimit;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "shortSession")
    private Boolean shortSession = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
