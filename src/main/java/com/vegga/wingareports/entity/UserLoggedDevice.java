package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_logged_devices")
public class UserLoggedDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "deviceId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceId;

    @Column(name = "imei")
    private String imei;

    @Column(name = "platform")
    private String platform;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "appVersion")
    private String appVersion;

    @Column(name = "manufacturer")
    private String manufacturer;

    @Column(name = "modelNumber")
    private String modelNumber;

    @Column(name = "osVersion")
    private String osVersion;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
