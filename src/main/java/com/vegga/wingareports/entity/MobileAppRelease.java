package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "mobile_app_releases")
public class MobileAppRelease implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 1 for android, 2 for ios
     */
    @Column(name = "platformId")
    private Integer platformId;

    @Column(name = "versionCode")
    private String versionCode;

    @Column(name = "versionName")
    private String versionName;

    /**
     * 1 for yes , 0 for no
     */
    @Column(name = "mandatory")
    private Integer mandatory = 0;

    @Column(name = "deleted")
    private Boolean deleted = Boolean.FALSE;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "description")
    private String description;

}
