package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_eligible_candidates")
public class CategoryEligibleCandidate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cecId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cecId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "cgsId")
    private Long cgsId;

    @Column(name = "weight")
    private Long weight = 1L;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
