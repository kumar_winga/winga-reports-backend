package com.vegga.wingareports.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Table(name = "client_user")
@Entity
@Data
public class ClientUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long clientUserId;
    private String userId;
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private Long cpId;
    private String cpNumber;
    private String clientName;
    private String contactNumber;
    private String email;
    private String role; //ROLE_USER{ read, edit }, ROLE_ADMIN {delete}
    private String[] authorities;
    private String profileImageUrl;
    private boolean active;
    private boolean notLocked;
    private Date joinDate;
    private Date lastLoginDate;
    private Date lastLoginDateDisplay;

}