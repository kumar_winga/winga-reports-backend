package com.vegga.wingareports.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class MetaFormFeildTypePK implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer fftId;
    private String name;
}
