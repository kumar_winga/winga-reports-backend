package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "meta_regions")
public class MetaRegion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "regionId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long regionId;

    @Column(name = "name")
    private String name;

}
