package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "form_contents")
public class FormContent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "fcId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fcId;

    @Column(name = "contentId", nullable = false)
    private Long contentId;

    @Column(name = "name")
    private String name;

    @Column(name = "formDescription")
    private String formDescription;

    @Column(name = "formButtonText")
    private String formButtonText;

    @Column(name = "alert1")
    private String alert1;

    @Column(name = "alert2")
    private String alert2;

    @Column(name = "alert3")
    private String alert3;

    @Column(name = "modifiedBy", nullable = false)
    private Long modifiedBy;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "createdTime")
    private Date createdTime;

}
