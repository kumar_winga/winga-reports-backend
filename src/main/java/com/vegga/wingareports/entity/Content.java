package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "contents")
public class Content implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "contentId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long contentId;

    @Column(name = "contentNumber")
    private String contentNumber;

    @Column(name = "contentUploadId")
    private String contentUploadId;

    @Column(name = "campaignId")
    private Long campaignId;

    @Column(name = "cpId")
    private Long cpId;

    @Column(name = "title")
    private String title;

    /**
     * 1 for youtube video,2 for audio,3 for image
     */
    @Column(name = "type")
    private Integer type;

    @Column(name = "priority")
    private Integer priority = 1;

    @Column(name = "geoLocked")
    private Boolean geoLocked = Boolean.FALSE;

    /**
     * 0 for unpublished,1 for published
     */
    @Column(name = "state")
    private Integer state = 0;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "playDuration")
    private Long playDuration = 0L;

    @Column(name = "uniquePlayDuration")
    private Long uniquePlayDuration = 0L;

    @Column(name = "noOfTimesVideoPlayed")
    private Integer noOfTimesVideoPlayed = 0;

    @Column(name = "timeTakenToAnswer")
    private Long timeTakenToAnswer = 0L;

    @Column(name = "totalEngagementTime")
    private Long totalEngagementTime = 0L;

    @Column(name = "viewCount")
    private Long viewCount;

    @Column(name = "suitableForMinors")
    private Boolean suitableForMinors = Boolean.TRUE;

    @Column(name = "suitableForChildren")
    private Integer suitableForChildren = 1;

    @Column(name = "bannerDuration")
    private Long bannerDuration;

    /**
     * 0 :both, 1 :male, 2 :female
     */
    @Column(name = "gender")
    private Integer gender = 0;

    @Column(name = "buyLinkEnabled")
    private Boolean buyLinkEnabled = Boolean.FALSE;

    @Column(name = "buyLinkButtonText")
    private String buyLinkButtonText;

    @Column(name = "buyLink")
    private String buyLink;

    @Column(name = "buyLinkAlertText")
    private String buyLinkAlertText;

    @Column(name = "buyLinkThumbnail")
    private Long buyLinkThumbnail;

    @Column(name = "form")
    private Boolean form = Boolean.FALSE;

    @Column(name = "survey")
    private Boolean survey = Boolean.FALSE;

    @Column(name = "surveyPoint")
    private Integer surveyPoint;

    @Column(name = "modifiedBy")
    private Long modifiedBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
