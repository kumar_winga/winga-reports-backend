package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_address")
public class UserAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "addressId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long addressId;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "adreTypeId")
    private Long adreTypeId;

    @Column(name = "countryId")
    private Long countryId;

    @Column(name = "stateId")
    private Long stateId;

    @Column(name = "city")
    private String city;

    @Column(name = "pincode")
    private String pincode;

    @Column(name = "addrIsDefault")
    private Integer addrIsDefault = 0;

    @Column(name = "deleted")
    private Integer deleted = 0;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

    @Column(name = "address1")
    private String address1;

    @Column(name = "address2")
    private String address2;

    @Column(name = "landmark")
    private String landmark;

}
