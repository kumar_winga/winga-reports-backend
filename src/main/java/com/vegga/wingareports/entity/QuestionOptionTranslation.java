package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "question_option_translations")
public class QuestionOptionTranslation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "oppId")
    private Long oppId;

    @Column(name = "langId")
    private Long langId;

    @Column(name = "txt")
    private String txt;

    @Column(name = "dirtyFlag")
    private Integer dirtyFlag = 0;

}
