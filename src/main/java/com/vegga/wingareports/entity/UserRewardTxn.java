package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_reward_txns")
public class UserRewardTxn implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "txnToken", nullable = false)
    private String txnToken;

    @Column(name = "txnHash", nullable = false)
    private String txnHash;

    @Column(name = "userId", nullable = false)
    private Long userId;

    /**
     * 1: loyality points, 2: referal, 3: instal bonus
     */
    @Column(name = "txnType", nullable = false)
    private Integer txnType;

}
