package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_realize_revenues")
public class CategoryRealizeRevenue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "crrId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long crrId;

    @Column(name = "cgsId")
    private Long cgsId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "instlRevenuBfrBonus")
    private Double instlRevenuBfrBonus = 0D;

    @Column(name = "instCount")
    private Long instCount = 0L;

    @Column(name = "instBonusAmt")
    private Double instBonusAmt = 0D;

    @Column(name = "instRevenueAftrBonus")
    private Double instRevenueAftrBonus = 0D;

    @Column(name = "viewCount")
    private Long viewCount = 0L;

    @Column(name = "viewRevenueBfrLoyality")
    private Double viewRevenueBfrLoyality = 0D;

    @Column(name = "amntSpentOnLoyality")
    private Double amntSpentOnLoyality = 0D;

    @Column(name = "viewRevenueAftrLoyality")
    private Double viewRevenueAftrLoyality = 0D;

    @Column(name = "amtDistributedAsPrize")
    private Double amtDistributedAsPrize = 0D;

    @Column(name = "finalBalanceAmt")
    private Double finalBalanceAmt = 0D;

    /**
     * 0: processing, 1: waiting, 2: final
     */
    @Column(name = "state")
    private Integer state = 0;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
