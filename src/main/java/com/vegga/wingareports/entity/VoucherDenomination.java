package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "voucher_denominations")
public class VoucherDenomination implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "denominationId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long denominationId;

    @Column(name = "voucherId")
    private Long voucherId;

    @Column(name = "denomination")
    private Double denomination;

    @Column(name = "createdTime")
    private Date createdTime;

}
