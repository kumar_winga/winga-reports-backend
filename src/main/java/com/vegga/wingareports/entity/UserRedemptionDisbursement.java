package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "user_redemptions_disbursements")
public class UserRedemptionDisbursement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "disbursementId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long disbursementId;

    @Column(name = "noOfUnqUsers")
    private Long noOfUnqUsers;

    @Column(name = "noOfTransactions")
    private Long noOfTransactions = 0L;

    @Column(name = "noOfToBeProcessed")
    private Double noOfToBeProcessed = 0D;

    @Column(name = "noOfSuccess")
    private Double noOfSuccess = 0D;

    @Column(name = "noOfPending")
    private Double noOfPending = 0D;

    @Column(name = "noOfFailed")
    private Double noOfFailed = 0D;

    @Column(name = "noOfRejected")
    private Double noOfRejected = 0D;

    @Column(name = "ttlAmtAsRefBonus")
    private Double ttlAmtAsRefBonus = 0D;

    @Column(name = "ttlAmtAsLoyality")
    private Double ttlAmtAsLoyality = 0D;

    @Column(name = "ttlAmtAsGift")
    private Double ttlAmtAsGift = 0D;

    @Column(name = "ttlAmtAsInstBonus")
    private Double ttlAmtAsInstBonus = 0D;

    @Column(name = "paymentPlatformFee")
    private Double paymentPlatformFee;

    @Column(name = "paymentPlatformFeeGst")
    private Double paymentPlatformFeeGst;

    @Column(name = "ttlAmt")
    private Double ttlAmt;

    @Column(name = "approvedBy")
    private Long approvedBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
