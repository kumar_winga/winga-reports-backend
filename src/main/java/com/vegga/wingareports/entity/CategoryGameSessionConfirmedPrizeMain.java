package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "category_game_session_confirmed_prize_main")
public class CategoryGameSessionConfirmedPrizeMain implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "cgscpmId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cgscpmId;

    @Column(name = "crrId")
    private Long crrId;

    @Column(name = "cgsId")
    private Long cgsId;

    @Column(name = "categoryId")
    private Long categoryId;

    @Column(name = "pctToDistribute")
    private Double pctToDistribute = 0D;

    @Column(name = "amtToDistribute")
    private Double amtToDistribute = 0D;

    @Column(name = "pointsToDistribute")
    private Long pointsToDistribute = 0L;

    @Column(name = "approved")
    private Boolean approved = Boolean.FALSE;

    @Column(name = "approvedBy")
    private Long approvedBy;

    @Column(name = "approvedTime")
    private Date approvedTime;

    @Column(name = "createdBy")
    private Long createdBy;

    @Column(name = "createdTime")
    private Date createdTime;

    @Column(name = "modifiedTime")
    private Date modifiedTime;

}
