package com.vegga.wingareports.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name = "notification_messages")
public class NotificationMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "type", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer type;

    @Column(name = "message")
    private String message;

    @Column(name = "title")
    private String title;

}
