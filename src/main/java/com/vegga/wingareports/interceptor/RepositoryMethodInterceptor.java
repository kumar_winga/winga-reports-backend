package com.vegga.wingareports.interceptor;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.aop.interceptor.CustomizableTraceInterceptor;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

public class RepositoryMethodInterceptor extends CustomizableTraceInterceptor {

    @Override
    protected Class getClassForLogging(Object target) {
        Class classForLogging = super.getClassForLogging(target);
        if (SimpleJpaRepository.class.equals(classForLogging)) {
            Class[] interfaces = AopProxyUtils.proxiedUserInterfaces(target);
            if (interfaces.length > 0) {
                return interfaces[0];
            }
        }
        return classForLogging;
    }

    protected void writeToLog(Log logger, String message, Throwable ex) {
        if (ex != null) {
            logger.info(message, ex);
        } else {
            logger.info(message);
        }
    }

    protected boolean isInterceptorEnabled(MethodInvocation invocation, Log logger) {
        return true;
    }
}
