package com.vegga.wingareports.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Widget {
    private String name;
    private Long count;
    private String icon;
    private String color;
    private String navigation;
}
