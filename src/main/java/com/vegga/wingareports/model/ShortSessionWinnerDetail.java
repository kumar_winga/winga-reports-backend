package com.vegga.wingareports.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShortSessionWinnerDetail {

    private Long userId;
    private String userName;
    private String mobileNumber;
    private String createdTime;

    public Long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getCreatedTime() {
        return createdTime;
    }
}
