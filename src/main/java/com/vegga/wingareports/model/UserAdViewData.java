package com.vegga.wingareports.model;

import com.vegga.wingareports.utility.StringUtils;

public class UserAdViewData {
    private Long userId;
    private String userName;
    private String userMobile;
    private String playedTime;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = StringUtils.maskMobileNumber(userMobile);
    }

    public String getPlayedTime() {
        return playedTime;
    }

    public void setPlayedTime(String playedTime) {
        this.playedTime = playedTime;
    }
}
