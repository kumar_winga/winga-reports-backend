package com.vegga.wingareports.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ShortSessionDetail {

    private Long cgsId;
    private String name;
    private String conductedTime;
    private Long noOfUsersParticipated;
    private Long noOfWinners;

}
