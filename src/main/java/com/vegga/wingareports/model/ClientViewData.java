package com.vegga.wingareports.model;

import lombok.Data;

@Data
public class ClientViewData {
    private Long clientId;
    private String clientName;
    private Long totalEngagementTime;
    private Long noOfUsersPlayed;
    private Long wingaLeads;
}
