package com.vegga.wingareports.model;

import lombok.Data;

@Data
public class IdName {
    private Long id;
    private String name;
}
