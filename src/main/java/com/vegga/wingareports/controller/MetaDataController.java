package com.vegga.wingareports.controller;

import com.vegga.wingareports.repository.*;
import com.vegga.wingareports.repository.projection.IdName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/meta")
@CrossOrigin
public class MetaDataController {

    @Autowired
    private MetaStateRepository metaStateRepository;

    @Autowired
    private ContentProviderRepository contentProviderRepository;

    @Autowired
    private CampaignGroupRepository campaignGroupRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private ContentRepository contentRepository;

    @GetMapping("/states")
    public ResponseEntity<List<IdName>> getStates() {
        return ResponseEntity.ok(metaStateRepository.findAllStates());
    }

    @GetMapping("/clients")
    public ResponseEntity<List<IdName>> getClients() {
        return ResponseEntity.ok(contentProviderRepository.findAllClients());
    }

    @GetMapping("/campaignGroups/{clientId}")
    public ResponseEntity<List<IdName>> getCampaignGroupsByClientId(@PathVariable("clientId") Long clientId) {
        return ResponseEntity.ok(campaignGroupRepository.findCampaignGroupsByCpId(clientId));
    }

    @GetMapping("/campaigns/campaignGroup/{campaignGrpId}")
    public ResponseEntity<List<IdName>> getCampaignsByCampaignGrpId(@PathVariable("campaignGrpId") Long campaignGrpId) {
        return ResponseEntity.ok(campaignRepository.findCampaignsByCampaignGrpId(campaignGrpId));
    }

    @GetMapping("/campaigns/client/{clientId}")
    public ResponseEntity<List<IdName>> getCampaignsByCpId(@PathVariable("clientId") Long clientId) {
        return ResponseEntity.ok(campaignRepository.findCampaignsByCpId(clientId));
    }

    @GetMapping("/contents/campaign/{campaignId}")
    public ResponseEntity<List<IdName>> findContentsByCampaignId(@PathVariable("campaignId") Long campaignId) {
        return ResponseEntity.ok(contentRepository.findContentsByCampaignId(campaignId));
    }

    @GetMapping("/shortSession/contentProviders")
    public ResponseEntity<List<IdName>> getAllShortSessionContentProviders() {
        return ResponseEntity.ok(contentProviderRepository.getAllShortSessionContentProviders());
    }
}
