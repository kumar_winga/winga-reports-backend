package com.vegga.wingareports.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Value("${hello.message}")
    private String message;

    @GetMapping("/message")
    public String getMessage() {
        return message;
    }

}
