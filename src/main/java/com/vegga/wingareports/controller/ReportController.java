package com.vegga.wingareports.controller;

import com.vegga.wingareports.model.ShortSessionDetail;
import com.vegga.wingareports.model.UserAdViewData;
import com.vegga.wingareports.model.Widget;
import com.vegga.wingareports.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/report")
@CrossOrigin
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping("/dashboard")
    public ResponseEntity<List<Widget>> getDashboardWidgets() {
        return ResponseEntity.ok(reportService.getDashboardWidgets());
    }

    @GetMapping("/clientViewData")
    public ResponseEntity<Map<String, Object>> getClientViewData(@RequestParam(name = "fromDate", required = false) String fromDate, @RequestParam(name = "toDate", required = false) String toDate) {
        return ResponseEntity.ok(reportService.getClientViewData(fromDate, toDate));
    }

    @GetMapping("/campaignViewData")
    public ResponseEntity<Map<String, Object>> getCampaignViewData(@RequestParam(name = "clientId", required = true) Long cpId, @RequestParam(name = "fromDate", required = false) String fromDate, @RequestParam(name = "toDate", required = false) String toDate) {
        return ResponseEntity.ok(reportService.getCampaignViewData(cpId, fromDate, toDate));
    }

    @GetMapping("/adViewData")
    public ResponseEntity<Map<String, Object>> getAdViewData(@RequestParam(name = "clientId", required = true) Long cpId, @RequestParam(name = "campaignId", required = true) Long campaignId, @RequestParam(name = "fromDate", required = false) String fromDate, @RequestParam(name = "toDate", required = false) String toDate) {
        return ResponseEntity.ok(reportService.getAdViewData(cpId, campaignId, fromDate, toDate));
    }

    @GetMapping("/userAdViewData")
    public ResponseEntity<List<UserAdViewData>> getUserAdViewData(@RequestParam(name = "adId", required = true) Long adId, @RequestParam(name = "fromDate", required = false) String fromDate, @RequestParam(name = "toDate", required = false) String toDate) {
        return ResponseEntity.ok(reportService.getUserAdViewData(adId, fromDate, toDate));
    }

//    @GetMapping("/clientUserUnmaskDetail")
//    public ResponseEntity<Map<String, Object>> getClientUserUnmaskDetail(@RequestParam("clientUserId") Long clientUserId, @RequestParam("cpId") Long cpId, @RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate) {
//        return ResponseEntity.ok(reportService.getClientUserUnmaskDetail(clientUserId, cpId, fromDate, toDate));
//    }

    @GetMapping("/unMaskedUserMobile")
    public ResponseEntity<Map<String, Object>> getUnMaskedUserMobile(@RequestParam("userId") Long userId, @RequestParam("clientUserId") Long clientUserId, @RequestParam("username") String username, @RequestParam("cpId") Long cpId, @RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate) {
        return ResponseEntity.ok(reportService.getUnMaskedUserMobile(userId, clientUserId, cpId, username, fromDate, toDate));
    }

    @GetMapping("/shortSessionDetails")
    public ResponseEntity<List<ShortSessionDetail>> getShortSessionDetails() {
        return ResponseEntity.ok(reportService.getShortSessionDetails());
    }

    @GetMapping("/shortSessionWinner/{cgsId}")
    public ResponseEntity<Map<String, Object>> getShortSessionWinnerDetailByCgsId(@PathVariable("cgsId") Long cgsId) {
        return ResponseEntity.ok(reportService.getShortSessionWinnerDetailByCgsId(cgsId));
    }

    @GetMapping("/shortSessionParticipatedUsers/{cgsId}")
    public ResponseEntity<Map<String, Object>> getShortSessionParticipatedUserDetailByCgsId(@PathVariable("cgsId") Long cgsId) {
        return ResponseEntity.ok(reportService.getShortSessionParticipatedUserDetailByCgsId(cgsId));
    }

}