package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameSessionConfirmedPrizeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameSessionConfirmedPrizeDetailRepository extends JpaRepository<CategoryGameSessionConfirmedPrizeDetail, Long>, JpaSpecificationExecutor<CategoryGameSessionConfirmedPrizeDetail> {

}