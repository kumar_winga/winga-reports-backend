package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserRewardTxn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRewardTxnRepository extends JpaRepository<UserRewardTxn, String>, JpaSpecificationExecutor<UserRewardTxn> {

}