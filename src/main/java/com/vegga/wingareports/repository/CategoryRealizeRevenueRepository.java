package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryRealizeRevenue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryRealizeRevenueRepository extends JpaRepository<CategoryRealizeRevenue, Long>, JpaSpecificationExecutor<CategoryRealizeRevenue> {

}