package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ClientUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientUserRepository extends JpaRepository<ClientUser, Long> {
    ClientUser findUserByUsername(String username);

    ClientUser findUserByEmail(String email);
}