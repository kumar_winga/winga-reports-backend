package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryActiveContentLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryActiveContentLocationRepository extends JpaRepository<CategoryActiveContentLocation, Long>, JpaSpecificationExecutor<CategoryActiveContentLocation> {

}