package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.InstallationRevenue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface InstallationRevenueRepository extends JpaRepository<InstallationRevenue, Long>, JpaSpecificationExecutor<InstallationRevenue> {

}