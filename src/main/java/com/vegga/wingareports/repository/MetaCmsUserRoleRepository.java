package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaCmsUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaCmsUserRoleRepository extends JpaRepository<MetaCmsUserRole, Long>, JpaSpecificationExecutor<MetaCmsUserRole> {

}