package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.VoucherDenomination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VoucherDenominationRepository extends JpaRepository<VoucherDenomination, Long>, JpaSpecificationExecutor<VoucherDenomination> {

}