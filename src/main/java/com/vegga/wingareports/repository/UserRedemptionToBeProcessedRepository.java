package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserRedemptionToBeProcessed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRedemptionToBeProcessedRepository extends JpaRepository<UserRedemptionToBeProcessed, Long>, JpaSpecificationExecutor<UserRedemptionToBeProcessed> {

}