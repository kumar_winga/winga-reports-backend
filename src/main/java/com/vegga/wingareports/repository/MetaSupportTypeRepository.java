package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaSupportType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaSupportTypeRepository extends JpaRepository<MetaSupportType, Long>, JpaSpecificationExecutor<MetaSupportType> {

}