package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ScheduleJobExecutionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ScheduleJobExecutionHistoryRepository extends JpaRepository<ScheduleJobExecutionHistory, Long>, JpaSpecificationExecutor<ScheduleJobExecutionHistory> {

}