package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.QuestionTranslation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface QuestionTranslationRepository extends JpaRepository<QuestionTranslation, Long>, JpaSpecificationExecutor<QuestionTranslation> {

}