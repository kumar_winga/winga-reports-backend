package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaTaluk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaTalukRepository extends JpaRepository<MetaTaluk, Long>, JpaSpecificationExecutor<MetaTaluk> {

}