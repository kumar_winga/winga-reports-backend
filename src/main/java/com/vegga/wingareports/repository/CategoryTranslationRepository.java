package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryTranslation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryTranslationRepository extends JpaRepository<CategoryTranslation, Long>, JpaSpecificationExecutor<CategoryTranslation> {

}