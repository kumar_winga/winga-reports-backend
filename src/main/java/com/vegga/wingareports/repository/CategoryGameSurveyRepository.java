package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameSurvey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameSurveyRepository extends JpaRepository<CategoryGameSurvey, Long>, JpaSpecificationExecutor<CategoryGameSurvey> {

}