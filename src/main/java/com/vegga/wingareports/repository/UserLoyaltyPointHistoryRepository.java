package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserLoyaltyPointHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserLoyaltyPointHistoryRepository extends JpaRepository<UserLoyaltyPointHistory, Long>, JpaSpecificationExecutor<UserLoyaltyPointHistory> {

}