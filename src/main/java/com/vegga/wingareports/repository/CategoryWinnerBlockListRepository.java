package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryWinnerBlockList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryWinnerBlockListRepository extends JpaRepository<CategoryWinnerBlockList, Long>, JpaSpecificationExecutor<CategoryWinnerBlockList> {

}