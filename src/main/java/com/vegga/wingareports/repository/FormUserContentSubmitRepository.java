package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.FormUserContentSubmit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FormUserContentSubmitRepository extends JpaRepository<FormUserContentSubmit, Long>, JpaSpecificationExecutor<FormUserContentSubmit> {

}