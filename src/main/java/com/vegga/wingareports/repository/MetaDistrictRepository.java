package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaDistrictRepository extends JpaRepository<MetaDistrict, Long>, JpaSpecificationExecutor<MetaDistrict> {

}