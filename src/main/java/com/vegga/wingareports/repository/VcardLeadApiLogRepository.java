package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.VcardLeadApiLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VcardLeadApiLogRepository extends JpaRepository<VcardLeadApiLog, Long>, JpaSpecificationExecutor<VcardLeadApiLog> {

}