package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryActiveContentBlockList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryActiveContentBlockListRepository extends JpaRepository<CategoryActiveContentBlockList, Long>, JpaSpecificationExecutor<CategoryActiveContentBlockList> {

}