package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.BlacklistedImeiNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BlacklistedImeiNumberRepository extends JpaRepository<BlacklistedImeiNumber, String>, JpaSpecificationExecutor<BlacklistedImeiNumber> {

}