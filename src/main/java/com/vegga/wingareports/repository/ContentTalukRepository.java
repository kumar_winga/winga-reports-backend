package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentTaluk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentTalukRepository extends JpaRepository<ContentTaluk, Long>, JpaSpecificationExecutor<ContentTaluk> {

}