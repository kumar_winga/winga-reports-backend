package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserAmtHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserAmtHistoryRepository extends JpaRepository<UserAmtHistory, Long>, JpaSpecificationExecutor<UserAmtHistory> {

}