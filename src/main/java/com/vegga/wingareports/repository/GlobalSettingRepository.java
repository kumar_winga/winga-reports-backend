package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.GlobalSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GlobalSettingRepository extends JpaRepository<GlobalSetting, Long>, JpaSpecificationExecutor<GlobalSetting> {

}