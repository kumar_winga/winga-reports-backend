package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.GenIdRange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GenIdRangeRepository extends JpaRepository<GenIdRange, Long>, JpaSpecificationExecutor<GenIdRange> {

}