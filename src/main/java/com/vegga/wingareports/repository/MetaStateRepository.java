package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaState;
import com.vegga.wingareports.repository.projection.IdName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MetaStateRepository extends JpaRepository<MetaState, Integer>, JpaSpecificationExecutor<MetaState> {

    @Query("select s.id, s.name from MetaState s")
    public List<Object[]> getAllStates();

    @Query("select ms.id as id, ms.name as name from MetaState ms")
    public List<IdName> findAllStates();
}