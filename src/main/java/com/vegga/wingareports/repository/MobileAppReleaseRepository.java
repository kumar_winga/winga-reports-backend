package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MobileAppRelease;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MobileAppReleaseRepository extends JpaRepository<MobileAppRelease, Long>, JpaSpecificationExecutor<MobileAppRelease> {

}