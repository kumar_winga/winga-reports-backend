package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.PaymentPlatformPaytmLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentPlatformPaytmLogRepository extends JpaRepository<PaymentPlatformPaytmLog, Long>, JpaSpecificationExecutor<PaymentPlatformPaytmLog> {

}