package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ScheduleJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ScheduleJobRepository extends JpaRepository<ScheduleJob, Long>, JpaSpecificationExecutor<ScheduleJob> {

}