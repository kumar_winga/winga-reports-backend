package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentDistrictRepository extends JpaRepository<ContentDistrict, Long>, JpaSpecificationExecutor<ContentDistrict> {

}