package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.AppFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AppFileRepository extends JpaRepository<AppFile, Long>, JpaSpecificationExecutor<AppFile> {

}