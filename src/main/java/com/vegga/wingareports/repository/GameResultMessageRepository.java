package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.GameResultMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GameResultMessageRepository extends JpaRepository<GameResultMessage, Long>, JpaSpecificationExecutor<GameResultMessage> {

}