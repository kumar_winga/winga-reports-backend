package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.User;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    @Query(value = "select count(u.userId) from User u join UserAuthority ua on u.userId = ua.userAuthorityPK.userId and ua.userAuthorityPK.role = 'ROLE_USER'")
    Long findAppUserCount();

    User findUserByUserId(Long userId);
}