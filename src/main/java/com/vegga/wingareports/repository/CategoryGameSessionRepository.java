package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface CategoryGameSessionRepository extends JpaRepository<CategoryGameSession, Long>, JpaSpecificationExecutor<CategoryGameSession> {

    @Query(value = "select concat(cgs.startTime, ' to ', cgs.endTime) from CategoryGameSession cgs where cgs.cgsId = ?1")
    String getCategoryGameSessionConductedTimeByCgsId(Long cgsId);
}