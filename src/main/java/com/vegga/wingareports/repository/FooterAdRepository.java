package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.FooterAd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FooterAdRepository extends JpaRepository<FooterAd, Long>, JpaSpecificationExecutor<FooterAd> {

}