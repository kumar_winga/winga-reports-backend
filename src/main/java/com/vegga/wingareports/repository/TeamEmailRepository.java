package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.TeamEmail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TeamEmailRepository extends JpaRepository<TeamEmail, Long>, JpaSpecificationExecutor<TeamEmail> {

}