package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaPincode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface MetaPincodeRepository extends JpaRepository<MetaPincode, Long>, JpaSpecificationExecutor<MetaPincode> {

    public List<MetaPincode> findAllByStateId(Long stateId);
}