package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentPincode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentPincodeRepository extends JpaRepository<ContentPincode, Long>, JpaSpecificationExecutor<ContentPincode> {

}