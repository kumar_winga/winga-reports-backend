package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameScratchCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameScratchCardRepository extends JpaRepository<CategoryGameScratchCard, Long>, JpaSpecificationExecutor<CategoryGameScratchCard> {

}