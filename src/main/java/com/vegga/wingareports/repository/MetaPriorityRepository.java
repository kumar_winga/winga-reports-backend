package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaPriority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaPriorityRepository extends JpaRepository<MetaPriority, Long>, JpaSpecificationExecutor<MetaPriority> {

}