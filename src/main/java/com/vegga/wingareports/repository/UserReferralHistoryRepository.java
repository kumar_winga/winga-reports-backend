package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserReferralHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserReferralHistoryRepository extends JpaRepository<UserReferralHistory, Long>, JpaSpecificationExecutor<UserReferralHistory> {

}