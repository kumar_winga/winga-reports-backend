package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CampaignGroup;
import com.vegga.wingareports.repository.projection.IdName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CampaignGroupRepository extends JpaRepository<CampaignGroup, Long>, JpaSpecificationExecutor<CampaignGroup> {

    @Query("select cg.campaignGroupId, cg.title from CampaignGroup cg where cg.cpId = ?1")
    public List<Object[]> findByCpId(Long cpId);

    @Query(value = "select cg.campaignGroupId as id, cg.title as name from CampaignGroup cg where cg.cpId = ?1")
    List<IdName> findCampaignGroupsByCpId(Long cpId);
}