package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.HomePageLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HomePageLocationRepository extends JpaRepository<HomePageLocation, Long>, JpaSpecificationExecutor<HomePageLocation> {

}