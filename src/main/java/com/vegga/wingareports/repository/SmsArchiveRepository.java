package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.SmsArchive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmsArchiveRepository extends JpaRepository<SmsArchive, Long>, JpaSpecificationExecutor<SmsArchive> {

}