package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaRegion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaRegionRepository extends JpaRepository<MetaRegion, Long>, JpaSpecificationExecutor<MetaRegion> {

}