package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentCategoryRepository extends JpaRepository<ContentCategory, Long>, JpaSpecificationExecutor<ContentCategory> {

}