package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaHelpDesk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaHelpDeskRepository extends JpaRepository<MetaHelpDesk, Long>, JpaSpecificationExecutor<MetaHelpDesk> {

}