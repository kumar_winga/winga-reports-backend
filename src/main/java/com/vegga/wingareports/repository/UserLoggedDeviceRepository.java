package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserLoggedDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserLoggedDeviceRepository extends JpaRepository<UserLoggedDevice, Long>, JpaSpecificationExecutor<UserLoggedDevice> {

}