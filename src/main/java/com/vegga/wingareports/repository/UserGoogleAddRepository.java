package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserGoogleAdd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserGoogleAddRepository extends JpaRepository<UserGoogleAdd, Long>, JpaSpecificationExecutor<UserGoogleAdd> {

}