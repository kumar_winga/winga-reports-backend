package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryUserGameSession;
import com.vegga.wingareports.entity.CategoryUserGameSessionPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryUserGameSessionRepository extends JpaRepository<CategoryUserGameSession, CategoryUserGameSessionPK>, JpaSpecificationExecutor<CategoryUserGameSession> {

}