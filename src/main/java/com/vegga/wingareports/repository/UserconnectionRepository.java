package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.Userconnection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserconnectionRepository extends JpaRepository<Userconnection, String>, JpaSpecificationExecutor<Userconnection> {

}