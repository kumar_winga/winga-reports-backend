package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryWinner;
import com.vegga.wingareports.repository.projection.ShortSessionWinnerDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryWinnerRepository extends JpaRepository<CategoryWinner, Long>, JpaSpecificationExecutor<CategoryWinner> {

    @Query(value = "select count(cw.categoryWinnerId) from CategoryWinner cw where cw.cgsId = ?1")
    Long getCategoryWinnerCountByCgsId(Long cgsId);

    @Query(value = "select u.userId as userId, u.name as userName, u.username as mobileNumber, cw.createdTime as createdTime from CategoryWinner cw join User u on cw.userId = u.userId where cw.cgsId = ?1")
    List<ShortSessionWinnerDetail> getShortSessionWinnerDetailByCgsId(Long cgsId);

    @Query(value = "select distinct cw.cgspId from CategoryWinner cw where cw.cgsId = ?1")
    Long getShortSessionCgspIdByCgsId(Long cgsId);
}