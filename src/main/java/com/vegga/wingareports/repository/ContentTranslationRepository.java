package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentTranslation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentTranslationRepository extends JpaRepository<ContentTranslation, Long>, JpaSpecificationExecutor<ContentTranslation> {

}