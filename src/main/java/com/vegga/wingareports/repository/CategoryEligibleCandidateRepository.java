package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryEligibleCandidate;
import com.vegga.wingareports.repository.projection.ShortSessionWinnerDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryEligibleCandidateRepository extends JpaRepository<CategoryEligibleCandidate, Long>, JpaSpecificationExecutor<CategoryEligibleCandidate> {

    @Query(value = "select count(cec.cecId) from CategoryEligibleCandidate cec where cec.cgsId = ?1")
    Long getCategoryEligibleCandidateCountByCgsId(Long cgsId);

}