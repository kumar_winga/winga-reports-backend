package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaAgeRange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaAgeRangeRepository extends JpaRepository<MetaAgeRange, Long>, JpaSpecificationExecutor<MetaAgeRange> {

}