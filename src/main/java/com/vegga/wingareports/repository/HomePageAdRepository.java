package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.HomePageAd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HomePageAdRepository extends JpaRepository<HomePageAd, Long>, JpaSpecificationExecutor<HomePageAd> {

}