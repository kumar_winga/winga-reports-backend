package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentBulkUploadProcess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentBulkUploadProcessRepository extends JpaRepository<ContentBulkUploadProcess, Long>, JpaSpecificationExecutor<ContentBulkUploadProcess> {

}