package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentBuyLinkLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface ContentBuyLinkLogRepository extends JpaRepository<ContentBuyLinkLog, Long>, JpaSpecificationExecutor<ContentBuyLinkLog> {

    @Query(value = "SELECT COUNT(*) FROM contents_buy_link_logs WHERE contentId = ?1 AND DATE(createdTime) = ?2", nativeQuery = true)
    Long getContentsBuyLinkClickCountByContentId(Long contentId, String date);
}