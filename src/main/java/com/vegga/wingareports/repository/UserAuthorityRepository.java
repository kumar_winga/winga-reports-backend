package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserAuthorityRepository extends JpaRepository<UserAuthority, String>, JpaSpecificationExecutor<UserAuthority> {

}