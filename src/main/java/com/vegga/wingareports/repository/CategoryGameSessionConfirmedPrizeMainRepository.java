package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameSessionConfirmedPrizeMain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameSessionConfirmedPrizeMainRepository extends JpaRepository<CategoryGameSessionConfirmedPrizeMain, Long>, JpaSpecificationExecutor<CategoryGameSessionConfirmedPrizeMain> {

}