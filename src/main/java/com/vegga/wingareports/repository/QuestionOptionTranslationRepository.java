package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.QuestionOptionTranslation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface QuestionOptionTranslationRepository extends JpaRepository<QuestionOptionTranslation, Long>, JpaSpecificationExecutor<QuestionOptionTranslation> {

}