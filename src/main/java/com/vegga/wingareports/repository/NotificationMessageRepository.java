package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.NotificationMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NotificationMessageRepository extends JpaRepository<NotificationMessage, Integer>, JpaSpecificationExecutor<NotificationMessage> {

}