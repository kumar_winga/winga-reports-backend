package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryActiveContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryActiveContentRepository extends JpaRepository<CategoryActiveContent, Long>, JpaSpecificationExecutor<CategoryActiveContent> {

}