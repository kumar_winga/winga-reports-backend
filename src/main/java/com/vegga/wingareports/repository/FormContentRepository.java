package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.FormContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FormContentRepository extends JpaRepository<FormContent, Long>, JpaSpecificationExecutor<FormContent> {

}