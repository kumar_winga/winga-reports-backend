package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameContentRepository extends JpaRepository<CategoryGameContent, Long>, JpaSpecificationExecutor<CategoryGameContent> {

}