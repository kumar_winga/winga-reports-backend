package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentDailyView;
import com.vegga.wingareports.repository.projection.AdViewData;
import com.vegga.wingareports.repository.projection.CampaignViewData;
import com.vegga.wingareports.repository.projection.ClientViewData;
import com.vegga.wingareports.repository.projection.ContentPerformanceData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContentDailyViewRepository extends JpaRepository<ContentDailyView, Long> {

    @Query(value = "SELECT category_games.cgsId, category_games.categoryId, category_game_contents.contentId, contents.cpId, contents.campaignId, campaigns.campaignGrpId, SUM(category_game_contents.playDuration) AS totalPlayDuration, SUM(category_game_contents.uniquePlayDuration) AS totalUniquePlayDuration, SUM(category_game_contents.noOfTimesVideoPlayed) AS totalNoOfTimesVideoPlayed, COUNT(category_game_contents.categoryGameId) AS noOfUniqueUsersPlayed, SUM(category_game_contents.timeTakenToAnswer) AS totalTimeTakenToAnswer, SUM(category_game_contents.totalEngagementTime) AS totalEngagementTime, SUM(category_game_contents.totalEngagementTimeActual) AS totalEngagementTimeActual, SUM(category_game_contents.viewCount) AS totalViewCount, category_game_sessions.createdTime FROM category_game_contents JOIN category_games ON category_game_contents.categoryGameId = category_games.categoryGameId JOIN category_game_sessions ON category_games.cgsId = category_game_sessions.cgsId JOIN contents ON category_game_contents.contentId = contents.contentId JOIN campaigns ON contents.campaignId = campaigns.campaignId WHERE category_games.cgsId = ?1 GROUP BY category_game_contents.contentId", nativeQuery = true)
    List<com.vegga.wingareports.repository.projection.ContentDailyView> getContentDailyViews(Long cgsId);

    @Query(value = "SELECT content_daily_views.cpId AS clientId, content_providers.name AS clientName, IF(content_providers.state = 1,'Active','InActive') AS state, SUM(content_daily_views.totalEngagementTime) AS totalEngagementTime, SUM(content_daily_views.totalNoOfTimesVideoPlayed) AS noOfUsersPlayed, SUM(content_daily_views.wingaLeads) AS wingaLeads FROM content_daily_views JOIN content_providers ON content_daily_views.cpId = content_providers.cpId WHERE DATE(content_daily_views.createdTime) >= ?1 AND DATE(content_daily_views.createdTime) <= ?2 GROUP BY content_daily_views.cpId", nativeQuery = true)
    List<ClientViewData> getClientViewData(String fromDate, String toDate);

    @Query(value = "SELECT campaigns.campaignId AS campaignId, campaigns.title AS campaignName, IF(campaigns.state = 2 ,'Active', 'InActive') AS state, SUM(content_daily_views.totalEngagementTime) AS totalEngagementTime, SUM(content_daily_views.totalNoOfTimesVideoPlayed) AS noOfUsersPlayed, SUM(content_daily_views.wingaLeads) AS wingaLeads FROM content_daily_views JOIN campaigns ON content_daily_views.campaignId = campaigns.campaignId WHERE content_daily_views.cpId = ?1 AND DATE(content_daily_views.createdTime) >= ?2 AND DATE(content_daily_views.createdTime) <= ?3 GROUP BY content_daily_views.campaignId", nativeQuery = true)
    List<CampaignViewData> getCampaignViewData(Long cpId, String fromDate, String toDate);

    @Query(value = "SELECT contents.contentId AS adId, contents.title AS adTitle, IF(contents.state = 1,'Published','UnPublished') AS state, SUM(content_daily_views.totalEngagementTime) AS totalEngagementTime, SUM(content_daily_views.totalNoOfTimesVideoPlayed) AS noOfUsersPlayed, SUM(content_daily_views.wingaLeads) AS wingaLeads FROM content_daily_views JOIN contents ON content_daily_views.contentId = contents.contentId WHERE content_daily_views.campaignId = ?1 AND DATE(content_daily_views.createdTime) >= ?2 AND DATE(content_daily_views.createdTime) <= ?3 GROUP BY content_daily_views.contentId", nativeQuery = true)
    List<AdViewData> getAdViewData(Long campaignId, String fromDate, String toDate);

    @Query(value = "SELECT content_providers.cpId AS clientId, content_providers.name AS clientName, contents.contentId AS contentId,contents.title AS contentTitle, SUM(content_daily_views.totalNoOfTimesVideoPlayed) AS totalNoOfTimesVideoPlayed, SUM(content_daily_views.totalViewCount) AS totalViewCount FROM content_daily_views JOIN content_providers ON content_daily_views.cpId = content_providers.cpId JOIN contents ON contents.contentId = content_daily_views.contentId WHERE content_daily_views.categoryId = ?1 AND contents.state = ?1 AND DATE(content_daily_views.createdTime) > ?2 AND DATE(content_daily_views.createdTime) < ?3 GROUP BY content_daily_views.contentId ORDER BY SUM(content_daily_views.totalNoOfTimesVideoPlayed)", nativeQuery = true)
    List<ContentPerformanceData> getContentPerformanceData(Long categoryId, String fromDate, String toDate);

    @Query(value = "select distinct cdv.cgsId from ContentDailyView cdv where cdv.categoryId = 5 and cdv.campaignId in (select distinct c.campaignId from Campaign c where c.shortSession = true)")
    List<Long> getShortSessionCategoryGameSessionIds();

    @Query(value = "select distinct cdv.campaignId from ContentDailyView cdv where cdv.cgsId = ?1")
    Long getShortSessionCampaignIdByCgsId(Long cgsId);
}
