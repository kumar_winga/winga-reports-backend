package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.Campaign;
import com.vegga.wingareports.repository.projection.IdName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CampaignRepository extends JpaRepository<Campaign, Long>, JpaSpecificationExecutor<Campaign> {

    @Query("select c.campaignId, c.title from Campaign c where c.campaignGrpId = ?1")
    public List<Object[]> findByCampaignGroupId(Long campaignGroupId);

    @Query("select c.campaignId, c.title from Campaign c where c.cpId = ?1")
    public List<Object[]> findByCampaignCpId(Long cpId);

    @Query(value = "select c.campaignId as id, c.title as name from Campaign c where c.campaignGrpId = ?1")
    List<IdName> findCampaignsByCampaignGrpId(Long campaignGrpId);

    @Query(value = "select c.campaignId as id, c.title as name from Campaign c where c.cpId = ?1")
    List<IdName> findCampaignsByCpId(Long cpId);

    @Query(value = "select count(c.shortSession) from Campaign c where c.shortSession = 1")
    Long getShortSessionCampaignCount();

    @Query(value = "select count(c.shortSession) from Campaign c where c.shortSession = true and c.cpId = ?1")
    Long getShortSessionCampaignCountByCpId(Long cpId);

    @Query(value = "select c.title from Campaign c where c.campaignId = (select distinct cdv.campaignId from ContentDailyView cdv where cdv.cgsId = ?1)")
    String getCampaignNameByCgsId(Long cgsId);

}