package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryCouponWinner;
import com.vegga.wingareports.repository.projection.ShortSessionWinnerDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryCouponWinnerRepository extends JpaRepository<CategoryCouponWinner, Long>, JpaSpecificationExecutor<CategoryCouponWinner> {

    @Query(value = "select count(distinct ccw.userId) from CategoryCouponWinner ccw where ccw.cgsId = ?1")
    Long getCategoryCouponWinnerCountByCgsId(Long cgsId);

    @Query(value = "select u.userId as userId, u.name as userName, u.username as mobileNumber, ccw.createdTime as createdTime from CategoryCouponWinner ccw join User u on ccw.userId = u.userId where ccw.cgsId = ?1")
    List<ShortSessionWinnerDetail> getShortSessionWinnerDetailByCgsId(Long cgsId);

    @Query(value = "select distinct ccw.cgspId from CategoryCouponWinner ccw where ccw.cgsId = ?1")
    Long getShortSessionCgspIdByCgsId(Long cgsId);
}
