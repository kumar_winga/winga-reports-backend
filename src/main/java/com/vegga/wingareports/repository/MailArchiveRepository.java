package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MailArchive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MailArchiveRepository extends JpaRepository<MailArchive, Long>, JpaSpecificationExecutor<MailArchive> {

}