package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.HomePageAdViewDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HomePageAdViewDetailRepository extends JpaRepository<HomePageAdViewDetail, Long>, JpaSpecificationExecutor<HomePageAdViewDetail> {

}