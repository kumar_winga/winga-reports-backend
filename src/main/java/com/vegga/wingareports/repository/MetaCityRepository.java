package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaCity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaCityRepository extends JpaRepository<MetaCity, Integer>, JpaSpecificationExecutor<MetaCity> {

}