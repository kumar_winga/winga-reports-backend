package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContentStateRepository extends JpaRepository<ContentState, Long>, JpaSpecificationExecutor<ContentState> {

}