package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContentProvider;
import com.vegga.wingareports.repository.projection.IdName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContentProviderRepository extends JpaRepository<ContentProvider, Long>, JpaSpecificationExecutor<ContentProvider> {

    @Query("select cp.cpId, cp.name from ContentProvider cp")
    public List<Object[]> getAllContentProviders();

    public ContentProvider getContentProviderByCpId(Long cpId);

    @Query(value = "select cp.cpId as id, cp.name as name from ContentProvider cp")
    List<IdName> findAllClients();

    @Query(value = "select cp.cpId as id, cp.name as name from Campaign c join ContentProvider cp on c.cpId = cp.cpId where c.shortSession = true")
    List<IdName> getAllShortSessionContentProviders();
}