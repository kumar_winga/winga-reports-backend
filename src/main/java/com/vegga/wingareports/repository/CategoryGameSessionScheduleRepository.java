package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameSessionSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameSessionScheduleRepository extends JpaRepository<CategoryGameSessionSchedule, Long>, JpaSpecificationExecutor<CategoryGameSessionSchedule> {

}