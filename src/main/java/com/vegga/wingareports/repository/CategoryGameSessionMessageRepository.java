package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameSessionMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameSessionMessageRepository extends JpaRepository<CategoryGameSessionMessage, Long>, JpaSpecificationExecutor<CategoryGameSessionMessage> {

}