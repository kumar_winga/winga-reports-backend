package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameResultMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameResultMessageRepository extends JpaRepository<CategoryGameResultMessage, Long>, JpaSpecificationExecutor<CategoryGameResultMessage> {

}