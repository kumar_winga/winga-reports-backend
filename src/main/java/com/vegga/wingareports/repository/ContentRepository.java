package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.Content;
import com.vegga.wingareports.repository.projection.IdName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContentRepository extends JpaRepository<Content, Long>, JpaSpecificationExecutor<Content> {

    @Query(value = "select sum(c.viewCount) from Content c")
    Long findTotalViewCount();

    @Query(value = "select c.contentId as id, c.title as name from Content c where c.campaignId = ?1 and c.state = 1")
    List<IdName> findContentsByCampaignId(Long campaignId);
}