package com.vegga.wingareports.repository.projection;

public interface ShortSessionWinnerDetail {

    public Long getUserId();

    public String getUserName();

    public String getMobileNumber();

    public String getCreatedTime();
}
