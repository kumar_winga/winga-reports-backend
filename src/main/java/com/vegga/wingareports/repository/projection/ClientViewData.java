package com.vegga.wingareports.repository.projection;

public interface ClientViewData {

    Long getClientId();
    String getClientName();
    String getState();
    Long getTotalEngagementTime();
    Long getNoOfUsersPlayed();
    Long getWingaLeads();
}
