package com.vegga.wingareports.repository.projection;

public interface ContentPerformanceData {

    Long getClientId();
    String getClientName();
    Long getContentId();
    String getContentTitle();
    Long getTotalNoOfTimesVideoPlayed();
    Long getTotalViewCount();
    
}
