package com.vegga.wingareports.repository.projection;

public interface IdName {

    Long getId();
    String getName();

}
