package com.vegga.wingareports.repository.projection;

public interface AdViewData {

    Long getAdId();
    String getAdTitle();
    String getState();
    Long getTotalEngagementTime();
    Long getNoOfUsersPlayed();
    Long getWingaLeads();

}
