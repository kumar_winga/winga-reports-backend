package com.vegga.wingareports.repository.projection;

public interface CampaignViewData {

    Long getCampaignId();
    String getCampaignName();
    String getState();
    Long getTotalEngagementTime();
    Long getNoOfUsersPlayed();
    Long getWingaLeads();

}
