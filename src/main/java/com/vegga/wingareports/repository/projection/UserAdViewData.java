package com.vegga.wingareports.repository.projection;

public interface UserAdViewData {

    Long getUserId();
    String getUserName();
    String getUserMobile();
    String getPlayedTime();

}
