package com.vegga.wingareports.repository.projection;

public interface ContentDailyView {

    Long getCgsId();
    Long getCategoryId();
    Long getContentId();
    Long getCpId();
    Long getCampaignId();
    Long getCampaignGrpId();
    Long getTotalPlayDuration();
    Long getTotalUniquePlayDuration();
    Long getTotalNoOfTimesVideoPlayed();
    Long getNoOfUniqueUsersPlayed();
    Long getTotalTimeTakenToAnswer();
    Long getTotalEngagementTime();
    Long getTotalEngagementTimeActual();
    Long getTotalViewCount();
    String getCreatedTime();
}
