package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaRedemptionOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaRedemptionOptionRepository extends JpaRepository<MetaRedemptionOption, Long>, JpaSpecificationExecutor<MetaRedemptionOption> {

}