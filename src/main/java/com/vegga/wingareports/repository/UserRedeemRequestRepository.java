package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserRedeemRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRedeemRequestRepository extends JpaRepository<UserRedeemRequest, Long>, JpaSpecificationExecutor<UserRedeemRequest> {

}