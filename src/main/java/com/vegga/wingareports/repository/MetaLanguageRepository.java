package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaLanguage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaLanguageRepository extends JpaRepository<MetaLanguage, Long>, JpaSpecificationExecutor<MetaLanguage> {

}