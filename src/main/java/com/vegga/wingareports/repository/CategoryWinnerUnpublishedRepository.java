package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryWinnerUnpublished;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryWinnerUnpublishedRepository extends JpaRepository<CategoryWinnerUnpublished, Long>, JpaSpecificationExecutor<CategoryWinnerUnpublished> {

}