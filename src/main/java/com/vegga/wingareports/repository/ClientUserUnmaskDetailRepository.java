package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ClientUserUnmaskDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface ClientUserUnmaskDetailRepository extends JpaRepository<ClientUserUnmaskDetail, Long> {

    @Modifying
    @Query(value = "update ClientUserUnmaskDetail set unMaskCount = unMaskCount + 1 where clientUserId = :clientUserId and cpId = :cpId and fromDate = :fromDate and toDate = :toDate")
    ClientUserUnmaskDetail updateClientUserUnmaskDetail(@Param("clientUserId") Long clientUserId, @Param("cpId") Long cpId, @Param("fromDate") Long fromDate, @Param("toDate") Long toDate);

    @Query(value = "SELECT * FROM client_user_unmask_details WHERE clientUserId = ?1 AND cpId = ?2 AND fromDate = ?3 AND toDate = ?4", nativeQuery = true)
    ClientUserUnmaskDetail getClientUserUnmaskDetail(Long clientUserId, Long cpId, String fromDate, String toDate);
//    Optional<ClientUserUnmaskDetail> getClientUserUnmaskDetail(@Param("clientUserId") Long clientUserId, @Param("cpId") Long cpId, @Param("fromDate") String fromDate, @Param("toDate") String toDate);

    @Modifying
    @Transactional
    @Query(value = "update ClientUserUnmaskDetail c set c.unMaskCount = c.unMaskCount + 1, c.modifiedTime = :modifiedTime where c.cuudId = :cuudId")
    int updateUnMaskCountInClientUserUnmaskDetailByCuudId(@Param("modifiedTime") String modifiedTime, @Param("cuudId") Long cuudId);
}
