package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.FormContentLableValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FormContentLableValueRepository extends JpaRepository<FormContentLableValue, Long>, JpaSpecificationExecutor<FormContentLableValue> {

}