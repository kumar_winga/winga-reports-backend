package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MailQueue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MailQueueRepository extends JpaRepository<MailQueue, Long>, JpaSpecificationExecutor<MailQueue> {

}