package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.UserRedemptionDisbursement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRedemptionDisbursementRepository extends JpaRepository<UserRedemptionDisbursement, Long>, JpaSpecificationExecutor<UserRedemptionDisbursement> {

}