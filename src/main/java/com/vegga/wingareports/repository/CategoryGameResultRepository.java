package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameResultRepository extends JpaRepository<CategoryGameResult, Long>, JpaSpecificationExecutor<CategoryGameResult> {

}