package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.ContactUs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ContactUsRepository extends JpaRepository<ContactUs, Long>, JpaSpecificationExecutor<ContactUs> {

}