package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.WinnerSpecialPrize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface WinnerSpecialPrizeRepository extends JpaRepository<WinnerSpecialPrize, Long>, JpaSpecificationExecutor<WinnerSpecialPrize> {

}