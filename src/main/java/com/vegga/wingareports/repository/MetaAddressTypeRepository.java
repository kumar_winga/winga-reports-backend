package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaAddressType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaAddressTypeRepository extends JpaRepository<MetaAddressType, Long>, JpaSpecificationExecutor<MetaAddressType> {

}