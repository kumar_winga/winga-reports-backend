package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.PaymentPlatformXoxoboxLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentPlatformXoxoboxLogRepository extends JpaRepository<PaymentPlatformXoxoboxLog, Long>, JpaSpecificationExecutor<PaymentPlatformXoxoboxLog> {

}