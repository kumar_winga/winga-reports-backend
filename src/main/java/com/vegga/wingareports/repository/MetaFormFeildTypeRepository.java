package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaFormFeildType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaFormFeildTypeRepository extends JpaRepository<MetaFormFeildType, String>, JpaSpecificationExecutor<MetaFormFeildType> {

}