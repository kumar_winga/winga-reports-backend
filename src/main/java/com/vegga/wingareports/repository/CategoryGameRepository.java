package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGame;
import com.vegga.wingareports.repository.projection.ShortSessionWinnerDetail;
import com.vegga.wingareports.repository.projection.UserAdViewData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryGameRepository extends JpaRepository<CategoryGame, Long>, JpaSpecificationExecutor<CategoryGame> {

    @Query(value = "SELECT category_games.userId AS userId, users.name AS userName, users.mobile AS userMobile, category_games.createdTime AS playedTime FROM category_games JOIN category_game_contents ON category_games.categoryGameId = category_game_contents.categoryGameId JOIN users ON users.userId = category_games.userId WHERE category_game_contents.contentId = ?1 AND DATE(category_games.createdTime) >= ?2 AND DATE(category_games.createdTime) <= ?3 ORDER BY category_games.createdTime DESC", nativeQuery = true)
    List<UserAdViewData> getUserAdViewData(Long contentId, String fromDate, String toDate);

    @Query(value = "select count(distinct cg.userId) from CategoryGame cg where cg.cgsId = ?1")
    Long getCategoryGameSessionUniqueUsersPlayed(Long cgsId);

    @Query(value = "select u.userId as userId, u.name as userName, u.username as mobileNumber, cg.createdTime as createdTime from CategoryGame cg join User u on cg.userId = u.userId where cg.cgsId = ?1")
    List<ShortSessionWinnerDetail> getShortSessionParticipatedUserDetailByCgsId(Long cgsId);
}