package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.FormContentLable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FormContentLableRepository extends JpaRepository<FormContentLable, Long>, JpaSpecificationExecutor<FormContentLable> {

}