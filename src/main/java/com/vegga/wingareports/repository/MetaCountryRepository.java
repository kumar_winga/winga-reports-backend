package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaCountry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaCountryRepository extends JpaRepository<MetaCountry, Integer>, JpaSpecificationExecutor<MetaCountry> {

}