package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.CategoryGameSessionPrize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CategoryGameSessionPrizeRepository extends JpaRepository<CategoryGameSessionPrize, Long>, JpaSpecificationExecutor<CategoryGameSessionPrize> {

}