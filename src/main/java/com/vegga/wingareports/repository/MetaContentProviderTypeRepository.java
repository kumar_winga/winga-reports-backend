package com.vegga.wingareports.repository;

import com.vegga.wingareports.entity.MetaContentProviderType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MetaContentProviderTypeRepository extends JpaRepository<MetaContentProviderType, Long>, JpaSpecificationExecutor<MetaContentProviderType> {

}