package com.vegga.wingareports.service;

import com.vegga.wingareports.entity.CategoryGameSession;
import com.vegga.wingareports.repository.CategoryGameSessionRepository;
import com.vegga.wingareports.repository.ContentBuyLinkLogRepository;
import com.vegga.wingareports.repository.ContentDailyViewRepository;
import com.vegga.wingareports.repository.projection.ContentDailyView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContentDailyViewService {

    @Autowired
    private CategoryGameSessionRepository categoryGameSessionRepository;

    @Autowired
    private ContentDailyViewRepository contentDailyViewRepository;

    @Autowired
    private ContentBuyLinkLogRepository contentBuyLinkLogRepository;


    public void saveContentDailyViews() {
        List<CategoryGameSession> categoryGameSessions = categoryGameSessionRepository.findAll();
        for (CategoryGameSession categoryGameSession : categoryGameSessions) {
            if (categoryGameSession.getCgsId() > 699 && categoryGameSession.getCgsId() < 716) {
                List<com.vegga.wingareports.entity.ContentDailyView> contentDailyViewEntities = new ArrayList<>();
                List<ContentDailyView> contentDailyViews = contentDailyViewRepository.getContentDailyViews(categoryGameSession.getCgsId());
                for (ContentDailyView contentDailyView : contentDailyViews) {
                    com.vegga.wingareports.entity.ContentDailyView contentDailyViewEntity = new com.vegga.wingareports.entity.ContentDailyView();
                    BeanUtils.copyProperties(contentDailyView, contentDailyViewEntity);
                    String date = contentDailyViewEntity.getCreatedTime().substring(0, contentDailyViewEntity.getCreatedTime().lastIndexOf(" "));
                    contentDailyViewEntity.setWingaLeads(contentBuyLinkLogRepository.getContentsBuyLinkClickCountByContentId(contentDailyViewEntity.getContentId(), date));
                    contentDailyViewEntities.add(contentDailyViewEntity);
                }
                contentDailyViewRepository.saveAll(contentDailyViewEntities);
                System.out.println(categoryGameSession.getCgsId()+" :: "+contentDailyViewEntities.size());
            }
        }

    }
}
