package com.vegga.wingareports.service;

import com.vegga.wingareports.entity.*;
import com.vegga.wingareports.model.ShortSessionDetail;
import com.vegga.wingareports.model.Widget;
import com.vegga.wingareports.repository.*;
import com.vegga.wingareports.repository.projection.*;
import com.vegga.wingareports.utility.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ContentProviderRepository contentProviderRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private ContentRepository contentRepository;

    @Autowired
    private CategoryGameRepository categoryGameRepository;

    @Autowired
    private ContentDailyViewRepository contentDailyViewRepository;

    @Autowired
    private ClientUserUnmaskDetailRepository clientUserUnmaskDetailRepository;

    @Autowired
    private CategoryGameSessionRepository categoryGameSessionRepository;

    @Autowired
    private CategoryWinnerRepository categoryWinnerRepository;

    @Autowired
    private CategoryCouponWinnerRepository categoryCouponWinnerRepository;

    @Autowired
    private CategoryEligibleCandidateRepository categoryEligibleCandidateRepository;

    @Autowired
    private CategoryGameSessionPrizeRepository categoryGameSessionPrizeRepository;

    public List<Widget> getDashboardWidgets() {
        List<Widget> widgets = new ArrayList<>();
        //Clients
        widgets.add(Widget.builder()
                .color("green-card")
                .count(contentProviderRepository.count())
                .name("Clients")
                .icon("star")
                .navigation("#")
                .build());
        //Campaigns
        widgets.add(Widget.builder()
                .color("blue-card")
                .count(campaignRepository.count())
                .name("Campaigns")
                .icon("book-open")
                .navigation("#")
                .build());

        //View Counts
        widgets.add(Widget.builder()
                .color("yellow-card")
                .count(contentRepository.findTotalViewCount())
                .name("Ad View Count")
                .icon("archive")
                .navigation("#")
                .build());
        //Users
        widgets.add(Widget.builder()
                .color("pink-card")
                .count(userRepository.findAppUserCount())
                .name("Users")
                .icon("bookmark")
                .navigation("#")
                .build());

        //ShortSessions
        widgets.add(Widget.builder()
                .color("yellow-card")
                .count(campaignRepository.getShortSessionCampaignCount())
//                .count(campaignRepository.getShortSessionCampaignCountByCpId(26l))
                .name("Short Sessions")
                .icon("star")
                .navigation("#")
                .build());

        return widgets;
    }

    public Map<String, Object> getClientViewData(String fromDate, String toDate) {
        Map<String, Object> response = new HashMap<>();
        LocalDate localDate = StringUtils.hasLength(toDate) ? LocalDate.parse(toDate) : LocalDate.now();
        toDate = localDate.toString();
        localDate = StringUtils.hasLength(fromDate) ? LocalDate.parse(fromDate) : localDate.minusDays(30);
        fromDate = localDate.toString();
        List<ClientViewData> clientViewData = contentDailyViewRepository.getClientViewData(fromDate, toDate);
        response.put("fromDate", fromDate);
        response.put("toDate", toDate);
        response.put("clientViewData", clientViewData);
        return response;
    }

    public Map<String, Object> getCampaignViewData(Long cpId, String fromDate, String toDate) {
        Map<String, Object> response = new HashMap<>();
        LocalDate localDate = StringUtils.hasLength(toDate) ? LocalDate.parse(toDate) : LocalDate.now();
        toDate = localDate.toString();
        localDate = StringUtils.hasLength(fromDate) ? LocalDate.parse(fromDate) : localDate.minusDays(30);
        fromDate = localDate.toString();
        List<CampaignViewData> campaignViewData = contentDailyViewRepository.getCampaignViewData(cpId, fromDate, toDate);
        Map<String, String> clientMap = new HashMap<>();
        ContentProvider contentProvider = contentProviderRepository.getContentProviderByCpId(cpId);
        clientMap.put("clientName", contentProvider.getName());
        clientMap.put("clientID", contentProvider.getCpNumber());
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        clientMap.put("onBoardDate", format.format(contentProvider.getCreatedTime()));
        response.put("clientData", clientMap);
        response.put("fromDate", fromDate);
        response.put("toDate", toDate);
        response.put("cpId", cpId);
        response.put("campaignViewData", campaignViewData);
        return response;
    }

    public Map<String, Object> getAdViewData(Long cpId, Long campaignId, String fromDate, String toDate) {
        Map<String, Object> response = new HashMap<>();
        LocalDate localDate = StringUtils.hasLength(toDate) ? LocalDate.parse(toDate) : LocalDate.now();
        toDate = localDate.toString();
        localDate = StringUtils.hasLength(fromDate) ? LocalDate.parse(fromDate) : localDate.minusDays(30);
        fromDate = localDate.toString();
        List<AdViewData> adViewData = contentDailyViewRepository.getAdViewData(campaignId, fromDate, toDate);
        Map<String, String> clientMap = new HashMap<>();
        ContentProvider contentProvider = contentProviderRepository.getContentProviderByCpId(cpId);
        clientMap.put("clientName", contentProvider.getName());
        clientMap.put("clientID", contentProvider.getCpNumber());
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        clientMap.put("onBoardDate", format.format(contentProvider.getCreatedTime()));
        response.put("clientData", clientMap);
        response.put("fromDate", fromDate);
        response.put("toDate", toDate);
        response.put("cpId", cpId);
        response.put("adViewData", adViewData);
        return response;
    }

    public List<com.vegga.wingareports.model.UserAdViewData> getUserAdViewData(Long adId, String fromDate, String toDate) {
        List<UserAdViewData> userAdViewDataProjection = categoryGameRepository.getUserAdViewData(adId, fromDate, toDate);
        List<com.vegga.wingareports.model.UserAdViewData> response = new ArrayList<>();
        for (UserAdViewData userAdViewData : userAdViewDataProjection) {
            com.vegga.wingareports.model.UserAdViewData userAdView = new com.vegga.wingareports.model.UserAdViewData();
            BeanUtils.copyProperties(userAdViewData, userAdView);
            response.add(userAdView);
        }
        return response;
    }

    public Map<String, Object> getUnMaskedUserMobile(Long userId, Long clientUserId, Long cpId, String username, String fromDate, String toDate) {
        Map<String, Object> response = new HashMap<>();
        ClientUserUnmaskDetail clientUserUnmaskDetail = clientUserUnmaskDetailRepository.getClientUserUnmaskDetail(clientUserId, cpId, fromDate, toDate);
        String mobileNumber = null;
        String message = "success";
        if (clientUserUnmaskDetail != null) {
            if (clientUserUnmaskDetail.getUnMaskCount() <= 5000) {
                User user = userRepository.findUserByUserId(userId);
                if (user != null) {
                    mobileNumber = user.getMobile();
                }
                clientUserUnmaskDetailRepository.updateUnMaskCountInClientUserUnmaskDetailByCuudId(DateTime.getNowInUTC(), clientUserUnmaskDetail.getCuudId());
                clientUserUnmaskDetail.setModifiedTime(DateTime.getNowInUTC());
                clientUserUnmaskDetail.setUnMaskCount(clientUserUnmaskDetail.getUnMaskCount() + 1);
            }else {
                message = "You have exceeded the maximum limit for un-masking the mobile number.";
            }
        } else {
            User user = userRepository.findUserByUserId(userId);
            if (user != null) {
                mobileNumber = user.getMobile();
            }
            clientUserUnmaskDetail = ClientUserUnmaskDetail.builder()
                    .clientUserId(clientUserId)
                    .cpId(cpId)
                    .username(username)
                    .fromDate(fromDate)
                    .toDate(toDate)
                    .unMaskCount(1)
                    .createdTime(DateTime.getNowInUTC())
                    .modifiedTime(DateTime.getNowInUTC())
                    .build();
            clientUserUnmaskDetail = clientUserUnmaskDetailRepository.save(clientUserUnmaskDetail);
        }
        response.put("clientUserUnmaskDetail", clientUserUnmaskDetail);
        response.put("mobileNumber", mobileNumber);
        response.put("message", message);
        return response;
    }

    public List<ShortSessionDetail> getShortSessionDetails() {
        List<ShortSessionDetail> shortSessionDetails = new ArrayList<>();
        List<Long> cgsIds = contentDailyViewRepository.getShortSessionCategoryGameSessionIds();
        for (Long cgsId : cgsIds) {
            String name = campaignRepository.getCampaignNameByCgsId(cgsId);
            String conductedTime = categoryGameSessionRepository.getCategoryGameSessionConductedTimeByCgsId(cgsId);
            Long noOfUsersParticipated = categoryGameRepository.getCategoryGameSessionUniqueUsersPlayed(cgsId);
            Long campaignId = contentDailyViewRepository.getShortSessionCampaignIdByCgsId(cgsId);
            Campaign campaign = campaignRepository.getById(campaignId);

            Long cgspId = categoryWinnerRepository.getShortSessionCgspIdByCgsId(cgsId);
            if (cgspId == null || cgsId ==0l) {
                cgspId = categoryCouponWinnerRepository.getShortSessionCgspIdByCgsId(cgsId);
            }
            CategoryGameSessionPrize categoryGameSessionPrize = categoryGameSessionPrizeRepository.getById(cgspId);
            Long noOfWinners = 0l;
            if (categoryGameSessionPrize.getType() == 5) {
                noOfWinners = categoryCouponWinnerRepository.getCategoryCouponWinnerCountByCgsId(cgsId);
            } else {
                noOfWinners = categoryWinnerRepository.getCategoryWinnerCountByCgsId(cgsId);
            }

            shortSessionDetails.add(ShortSessionDetail.builder()
                    .cgsId(cgsId)
                    .name(name)
                    .conductedTime(conductedTime)
                    .noOfUsersParticipated(noOfUsersParticipated)
                    .noOfWinners(noOfWinners)
                    .build());
        }
        return shortSessionDetails;
    }

    public Map<String, Object> getShortSessionWinnerDetailByCgsId(Long cgsId) {
        Map<String, Object> response = new HashMap<>();
        Long campaignId = contentDailyViewRepository.getShortSessionCampaignIdByCgsId(cgsId);
        Campaign campaign = campaignRepository.getById(campaignId);
        response.put("shortSessionName", campaign.getTitle());
        CategoryGameSession categoryGameSession = categoryGameSessionRepository.getById(cgsId);
        response.put("shortSessionStartTime", categoryGameSession.getStartTime());
        response.put("shortSessionEndTime", categoryGameSession.getEndTime());
        Long noOfUsersParticipated = categoryGameRepository.getCategoryGameSessionUniqueUsersPlayed(cgsId);
        response.put("shortSessionNoOfUsersParticipated", noOfUsersParticipated);
        List<ShortSessionWinnerDetail> shortSessionWinnerDetails = null;
        Long cgspId = categoryWinnerRepository.getShortSessionCgspIdByCgsId(cgsId);
        if (cgspId == null || cgsId ==0l) {
            cgspId = categoryCouponWinnerRepository.getShortSessionCgspIdByCgsId(cgsId);
        }
        CategoryGameSessionPrize categoryGameSessionPrize = categoryGameSessionPrizeRepository.getById(cgspId);
        if (categoryGameSessionPrize.getType() == 5) {
            shortSessionWinnerDetails = categoryCouponWinnerRepository.getShortSessionWinnerDetailByCgsId(cgsId);
        } else {
            shortSessionWinnerDetails = categoryWinnerRepository.getShortSessionWinnerDetailByCgsId(cgsId);
        }
        response.put("shortSessionNoOfWinners", shortSessionWinnerDetails.size());
        response.put("shortSessionWinners", shortSessionWinnerDetails);
        return response;
    }

    public Map<String, Object> getShortSessionParticipatedUserDetailByCgsId(Long cgsId) {
        Map<String, Object> response = new HashMap<>();
        Long campaignId = contentDailyViewRepository.getShortSessionCampaignIdByCgsId(cgsId);
        Campaign campaign = campaignRepository.getById(campaignId);
        response.put("shortSessionName", campaign.getTitle());
        CategoryGameSession categoryGameSession = categoryGameSessionRepository.getById(cgsId);
        response.put("shortSessionStartTime", categoryGameSession.getStartTime());
        response.put("shortSessionEndTime", categoryGameSession.getEndTime());
        Long noOfUsersParticipated = categoryGameRepository.getCategoryGameSessionUniqueUsersPlayed(cgsId);
        response.put("shortSessionNoOfUsersParticipated", noOfUsersParticipated);
        List<ShortSessionWinnerDetail> shortSessionWinnerDetails = categoryGameRepository.getShortSessionParticipatedUserDetailByCgsId(cgsId);
        response.put("shortSessionNoOfWinners", shortSessionWinnerDetails.size());
        response.put("shortSessionWinners", shortSessionWinnerDetails);
        return response;
    }
}
